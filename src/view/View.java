package view;


import java.awt.BorderLayout;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import com.teamdev.jxmaps.javafx.MapView;

import controller.Controller;
import model.data_structures.ArcoNumerico;
import model.data_structures.HashTableSeparateChaining;
import model.data_structures.LinkedList;
import model.data_structures.Lista.Lista;
import model.data_structures.grafo.ElCamino;
import model.data_structures.grafo.IArco;
import model.data_structures.grafo.IVertice;
import model.data_structures.grafo.grafoDirigido.GrafoDirigido;
import model.data_structures.grafo.grafoDirigido.Vertice;
import model.data_structures.grafo.grafoNoDirigido.GrafoNoDirigido;
import model.logic.Dibujar;
import model.vo.ComponenteConexo;
import model.vo.JXmapsB1;
import model.vo.JXmapsC3;
import model.vo.JXmapsc23;
import model.vo.Jxmaps;
import model.vo.Trip;
import model.vo.VerticeStation;


public class View extends MapView {
	private static Scanner linea;


	public double getDistance (double lat1, double lon1, double lat2, double lon2)  {   
		// TODO Auto-generated method stub    
		final int R = 6371*1000; // Radious of the earth in meters    
		Double latDistance = Math.toRadians(lat2-lat1);     
		Double lonDistance = Math.toRadians(lon2-lon1);     
		Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance/2) *  Math.sin(lonDistance/2);   
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));     Double distance = R * c;  
		return distance;
	}
	public static void main(String[] args){

		linea = new Scanner(System.in);
		boolean fin = false; 
		Controller controlador = new Controller();
		int option;
		GrafoDirigido<Integer, IVertice<Integer>, IArco> grafoc3=null;
		while(!fin)
		{



			printMenu();
			option = linea.nextInt();

			switch(option)
			{

			case 0:  //Carga de datos
				String dataTrips = "";  // ruta del archivo de Trips
				String dataStations = ""; // ruta del archivo de Stations
				boolean reiniciarDatos = false;



				if (!reiniciarDatos)
				{
					System.out.println("Trips x cargar al sistema: " + dataTrips);
					System.out.println("Stations x cargar al sistema: " + dataStations);
				}

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.currentTimeMillis();

				//Metodo 1C
				Controller.cargarDatos();
				GrafoNoDirigido<Integer, IVertice<Integer>, IArco> grafo = controlador.darGrafo();
				System.out.println("Total vertices cargados en el sistema:"+grafo.darVertices().size());
				System.out.println("Total arcos cargadas en el sistema:" + grafo.darNArcos() );


				//Tiempo en cargar
				long endTime = System.currentTimeMillis();
				long duration = endTime - startTime;

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;
			case 1:
				System.out.println("Ingrese numero de estaciones: (Ej: 56)");
				int nStations = Integer.parseInt(linea.next());
				Lista<VerticeStation> b1 = controlador.darNStationsMasCongestionadas(nStations);
				for (int i = 0; i < b1.darLongitud(); i++) {
					System.out.println("Nombre de la estacion:"+b1.darElemento(i).getStationName());
					System.out.println("Cantidad Viajes:"+b1.darElemento(i).darCantidadViajes());
					System.out.println("Cantidad Viajes que salieron:"+b1.darElemento(i).darCantidadViajesSalieron());
					System.out.println("Cantidad Viajes que llegaron:"+b1.darElemento(i).darCantidadViajesLLegaron());

				}
				JXmapsB1 mapb1 = new JXmapsB1(b1);
				JFrame framesb1 = new JFrame("Estaciones Concurridas");
				framesb1.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
				framesb1.add(mapb1, BorderLayout.CENTER);
				framesb1.setSize(700, 500);
				framesb1.setLocationRelativeTo(null);
				framesb1.setVisible(true);
				break;
			case 2:
				System.out.println("Ingrese numero de estaciones: (Ej: 56)");
				int nnStations = Integer.parseInt(linea.next());
				ElCamino<Integer, IVertice<Integer>, IArco> rta2 = controlador.darRutasMinimasNStations(nnStations);
				System.out.println("El costo total es:"+rta2.darCosto()+"Longitud del camino:"+rta2.darLongitud());
			case 3:
				System.out.println("Ingrese la latitud inicial: ");
				double a =Double.parseDouble(linea.next());

				System.out.println("Ingrese la longitud inicial: ");
				double b =Double.parseDouble(linea.next());

				System.out.println("Ingrese la latitud final: ");
				double c =Double.parseDouble(linea.next());

				System.out.println("Ingrese la longitud final: ");
				double d =Double.parseDouble(linea.next());

				LinkedList<String> lista1 =Controller.darViajeDeCostoMinimo(a, b, c, d);
				Dibujar dib2= new Dibujar();
				dib2.dibujarA1(lista1);
			case 5:
				grafoc3 = controlador.darGrafoC3();
				Lista<Trip> listastations = controlador.darListaTrips();

				System.out.println("Cantidad Vertices Cargados:"+grafoc3.darOrden());	
				System.out.println("Cantidad Arcos Cargados:"+grafoc3.darNArcos());
				JXmapsc23 map = new JXmapsc23(grafoc3, listastations);
				JFrame frames = new JFrame("GrafoDirigido");
				frames.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
				frames.add(map, BorderLayout.CENTER);
				frames.setSize(700, 500);
				frames.setLocationRelativeTo(null);
				frames.setVisible(true);


				break;
			case 6:

				Lista<ComponenteConexo> rta = controlador.componentesConexos();
				HashTableSeparateChaining<ArcoNumerico, Lista<IVertice<Integer>>> hash = controlador.darArcos();
				for (int i = 0; i < rta.darLongitud(); i++) 
				{
					try {
						Vertice x =(Vertice) rta.darElemento(i).darVertices().darElemento(0);
						if(rta.darElemento(i).getNumVertices()>0) {
							System.out.println("Cantidad de Vertices del componente conexo:"+rta.darElemento(i).getNumVertices());
							System.out.println("Id Del primer vertice en la lista de componente conexo:"+x.darId() );}
					}

					catch(Exception e)
					{
					}
				}
				System.out.println("cantidad de componentes conexos:"+rta.darLongitud());
				Jxmaps map2 = new Jxmaps(grafoc3, rta, hash);
				JFrame frames2 = new JFrame("ComponentesConexos");
				frames2.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
				frames2.add(map2, BorderLayout.CENTER);
				frames2.setSize(700, 500);
				frames2.setLocationRelativeTo(null);
				frames2.setVisible(true);


				break;
			case 7:
				Lista<Trip> listastationsw = controlador.darListaTrips();
				Lista<ComponenteConexo> lstaaa=controlador.componentesConexos();
				JXmapsC3 m = new JXmapsC3(grafoc3, listastationsw,lstaaa);
				JFrame f = new JFrame("Mapa todo");
				f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
				f.add(m, BorderLayout.CENTER);
				f.setSize(700, 500);
				f.setLocationRelativeTo(null);
				f.setVisible(true);
				break;



			}
		}


	}


	private static void printMenu()
	{
		System.out.println("-----------------ISIS 1206 - Estructuras de Datos------======----");
		System.out.println("-------------------- Proyecto 2   - 2018-2 ----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println("0. Cargar datos de todos los archivos");
		System.out.println("PARTE B");
		System.out.println("1. Dar N Estaciones mas congestionadas");
		System.out.println("2. Dar Ruta minima entre n estaciones");
		System.out.println("PARTE A");
		System.out.println("3. Viaje menor costo");
		System.out.println("4. Camino menos corto desde un lugar");
		System.out.println("PARTE C");
		System.out.println("5. Crear Grafo Dirigido C3");
		System.out.println("6. Dar ComponentesConexos");
		System.out.println("7. Pintar grafo");


	}




}
