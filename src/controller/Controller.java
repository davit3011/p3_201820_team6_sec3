package controller;


import API.IManager;
import model.data_structures.ArcoNumerico;
import model.data_structures.HashTableSeparateChaining;
import model.data_structures.LinkedList;
import model.data_structures.Lista.Lista;
import model.data_structures.grafo.ElCamino;
import model.data_structures.grafo.IArco;
import model.data_structures.grafo.IVertice;
import model.data_structures.grafo.grafoDirigido.GrafoDirigido;
import model.data_structures.grafo.grafoNoDirigido.GrafoNoDirigido;
import model.logic.Manager;
import model.vo.ComponenteConexo;
import model.vo.Trip;
import model.vo.VerticeStation;


public class Controller {
	private static IManager manager = new Manager();
public GrafoDirigido<Integer, IVertice<Integer>, IArco> darGrafoC3()
{
	return manager.grafoC3();
}
	public static void cargarDatos(){
		manager.cargarDatos();
	}
	public GrafoNoDirigido<Integer, IVertice<Integer>, IArco> darGrafo()
	{
		return manager.darGrafo();
	}
	public void grafoJSON()
	{
		manager.grafoJson();
	}
	
	public Lista<VerticeStation> darNStationsMasCongestionadas(int nStations)
	{
		return manager.StationsMasCongestionadas(nStations);
	}
	public ElCamino<Integer, IVertice<Integer>, IArco> darRutasMinimasNStations(int lista)
	{
		return manager.darRutasMinimas(lista);
	}
	public Lista<ComponenteConexo> componentesConexos()
	{
		return manager.C2ComponentesConexos();
	}
	public Lista<Trip> darListaTrips() {
		// TODO Auto-generated method stub
		return manager.darListaTrips();
	}
	public HashTableSeparateChaining<ArcoNumerico, Lista<IVertice<Integer>>> darArcos()
	{
		return manager.darArcos();
	}
	
	public static LinkedList<String> darViajeMasCorto(double latInicial, double lonInicial, double latFinal, double lonFinal) throws Exception
	{
		return manager.darViajeMasCorto(latInicial, lonInicial, latFinal, lonFinal);
	}
	
	public static LinkedList<String> darViajeDeCostoMinimo(double latInicial, double lonInicial, double latFinal, double lonFinal)
	{
		 return manager.darViajeDeCostoMinimo(latInicial, lonInicial, latFinal, lonFinal);
	}





}
