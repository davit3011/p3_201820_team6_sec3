package API;



import model.data_structures.ArcoNumerico;
import model.data_structures.HashTableSeparateChaining;
import model.data_structures.LinkedList;
import model.data_structures.Lista.Lista;
import model.data_structures.grafo.ElCamino;
import model.data_structures.grafo.IArco;
import model.data_structures.grafo.IVertice;
import model.data_structures.grafo.grafoDirigido.GrafoDirigido;
import model.data_structures.grafo.grafoNoDirigido.GrafoNoDirigido;
import model.vo.ComponenteConexo;
import model.vo.Trip;
import model.vo.VerticeStation;

public interface IManager {

	void cargarDatos();

	GrafoNoDirigido<Integer, IVertice<Integer>, IArco> darGrafo();

	void grafoJson();

	Lista<VerticeStation> StationsMasCongestionadas(int nStations);


	GrafoDirigido<Integer, IVertice<Integer>, IArco> grafoC3();

	@SuppressWarnings("rawtypes")
	Lista<ComponenteConexo> C2ComponentesConexos();

	Lista<Trip> darListaTrips();

	HashTableSeparateChaining<ArcoNumerico, Lista<IVertice<Integer>>> darArcos();

	ElCamino<Integer, IVertice<Integer>, IArco> darRutasMinimas(int nStations);
	
	LinkedList<String> darViajeMasCorto(double latInicial, double lonInicial, double latFinal, double lonFinal) throws Exception;
	
	LinkedList<String> darViajeDeCostoMinimo(double latInicial, double lonInicial, double latFinal, double lonFinal);
}
