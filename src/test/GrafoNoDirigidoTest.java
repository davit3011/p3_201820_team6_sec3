package test;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import com.google.gson.Gson;
import com.opencsv.CSVReader;

import API.IManager;
import junit.framework.TestCase;
import model.data_structures.ArcoNumerico;
import model.data_structures.DoubleLinkedList;
import model.data_structures.HashTableSeparateChaining;
import model.data_structures.RedBlackTree;
import model.data_structures.VerticeEstacion;
import model.data_structures.VerticeNumerico;
import model.data_structures.Lista.Lista;
import model.data_structures.grafo.ArcoNoExisteException;
import model.data_structures.grafo.ArcoYaExisteException;
import model.data_structures.grafo.IArco;
import model.data_structures.grafo.IVertice;
import model.data_structures.grafo.VerticeNoExisteException;
import model.data_structures.grafo.VerticeYaExisteException;
import model.data_structures.grafo.grafoNoDirigido.GrafoNoDirigido;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;
import model.vo.VerticeStation;
/**
 * Clase utilizada para verificar el funcionamiento de la clase GrafoNoDirigido
 * @see GrafoNoDirigido
 */
public class GrafoNoDirigidoTest extends TestCase
{
    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------
	
    /**
     * Grafo sobre el que se van a hacer las pruebas.
     */
    private GrafoNoDirigido<Integer, VerticeNumerico, ArcoNumerico> grafo;

    // -----------------------------------------------------------------
    // Escenarios de prueba
    // -----------------------------------------------------------------

    /**
     * Crea un grafo vacio
     */
    public void setupEscenario1( )
    {
        // Crear al grafo vacio
        grafo = new GrafoNoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );
    }

    /**
     * Crea un grafo con 5 v�rtices y 0 arcos
     */
    public void setupEscenario2( )
    {
        // Crear el grafo vacio
        grafo = new GrafoNoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

        // Crear los v�rtices
        try
        {
            VerticeNumerico vn = new VerticeNumerico( 0,0,0 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 1,1,1 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 2,2,2 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 3,3,3 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 4,4,4 );
            grafo.agregarVertice( vn );
        }
        catch( VerticeYaExisteException e )
        {
            // Esto no deber��a suceder
            fail( );
        }
    }

    /**
     * Crea un grafo con 200 v�rtices y 0 arcos
     */
    public void setupEscenario3( )
    {
        // Crear el grafo vacio
        grafo = new GrafoNoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

        // Crear los v�rtices
        try
        {
            for( int i = 0; i < 200; i++ )
            {
                // Encontrar un v�rtice no presente en el grafo y agregarlo
                int vertice;
                do
                {
                    vertice = ( int ) ( Math.random( ) * 200 );
                } while( grafo.existeVertice( vertice ) );
                grafo.agregarVertice( new VerticeNumerico( vertice,1,1 ) );
            }
        }
        catch( VerticeYaExisteException e )
        {
            // Esto no deber��a suceder
            fail( );
        }
    }

    /**
     * Crea un grafo con 5 v�rtices en forma de lista encadenada
     */
    public void setupEscenario4( )
    {
        // Crear el grafo vacio
        grafo = new GrafoNoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

        // Crear los v�rtices
        try
        {
            VerticeNumerico vn = new VerticeNumerico( 0,0,0 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 1,1,1 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 2,2,2 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 3,3,3 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 4,4,4 );
            grafo.agregarVertice( vn );
        }
        catch( VerticeYaExisteException e )
        {
            // Esto no deber�a suceder
            fail( );
        }

        // Agregar los v�rtices
        try
        {
            ArcoNumerico an = new ArcoNumerico( 7 );
            grafo.agregarArco( 0, 1, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 1, 2, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 2, 3, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 3, 4, an );
        }
        catch( VerticeNoExisteException e )
        {
            // Esto no deber��a
            fail( );
        }
        catch( ArcoYaExisteException e )
        {
            // Esto no deber��a
            fail( );
        }
    }

    /**
     * Crea un grafo con 6 v�rtices y 6 arcos
     */
    public void setupEscenario5( )
    {
        // Crear el grafo vacio
        grafo = new GrafoNoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

        // Crear los v�rtices y los arcos
        try
        {
            VerticeNumerico vn = new VerticeNumerico( 0,0,0 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 1,1,1 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 2,2,2 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 3,3,3 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 4,4,4 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 5,5,5 );
            grafo.agregarVertice( vn );

            ArcoNumerico an = new ArcoNumerico( 7 );
            grafo.agregarArco( 0, 1, an );
            an = new ArcoNumerico( 7 );
            grafo.agregarArco( 1, 2, an );
            an = new ArcoNumerico( 7 );
            grafo.agregarArco( 2, 3, an );
            an = new ArcoNumerico( 7 );
            grafo.agregarArco( 3, 4, an );
            an = new ArcoNumerico( 7 );
            grafo.agregarArco( 4, 1, an );
            an = new ArcoNumerico( 7 );
            grafo.agregarArco( 2, 5, an );
        }
        catch( VerticeYaExisteException e )
        {
            // Esto no deber��a suceder
            fail( );
        }
        catch( VerticeNoExisteException e )
        {
            // Esto no deber��a suceder
            fail( );
        }
        catch( ArcoYaExisteException e )
        {
            // Esto no deber��a suceder
            fail( );
        }
    }

    /**
     * Crea un grafo con 9 v�rtices y 10 arcos
     */
    public void setupEscenario6( )
    {
        grafo = new GrafoNoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

        try
        {
            VerticeNumerico vn = new VerticeNumerico( 0,0,0 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 1,1,1 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 2,2,2 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 3,3,3 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 4,4,4 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 5,5,5 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 6,6,6 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 7,7,7 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 8,8,8 );
            grafo.agregarVertice( vn );

            ArcoNumerico an = new ArcoNumerico( 1 );
            grafo.agregarArco( 0, 1, an );
            an = new ArcoNumerico( 201 );
            grafo.agregarArco( 1, 2, an );
            an = new ArcoNumerico( 2 );
            grafo.agregarArco( 2, 3, an );
            an = new ArcoNumerico( 3 );
            grafo.agregarArco( 0, 4, an );
            an = new ArcoNumerico( 80 );
            grafo.agregarArco( 4, 5, an );
            an = new ArcoNumerico( 130 );
            grafo.agregarArco( 5, 6, an );
            an = new ArcoNumerico( 5 );
            grafo.agregarArco( 6, 3, an );
            an = new ArcoNumerico( 34 );
            grafo.agregarArco( 4, 7, an );
            an = new ArcoNumerico( 55 );
            grafo.agregarArco( 7, 8, an );
            an = new ArcoNumerico( 21 );
            grafo.agregarArco( 8, 6, an );
        }
        catch( VerticeYaExisteException e )
        {
            // Esto no deber��a suceder
            fail( );
        }
        catch( VerticeNoExisteException e )
        {
            // Esto no deber��a suceder
            fail( );
        }
        catch( ArcoYaExisteException e )
        {
            // Esto no deber��a suceder
            fail( );
        }
    }
    
    /**
     * Crea un grafo no conexo con 5 v�rtices y 4 arcos
     */
    public void setupEscenario7( )
    {
        // Crear el grafo vac�o
        grafo = new GrafoNoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

        // Crear los v�rtices y los arcos
        try
        {
            VerticeNumerico vn = new VerticeNumerico( 0,0,0 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 1,1,1 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 2,2,2 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 3,3,3 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 4,4,4 );
            grafo.agregarVertice( vn );

            ArcoNumerico an = new ArcoNumerico( 7 );
            grafo.agregarArco( 0, 1, an );
            an = new ArcoNumerico( 7 );
            grafo.agregarArco( 1, 2, an );
            an = new ArcoNumerico( 7 );
            grafo.agregarArco( 0, 2, an );
            an = new ArcoNumerico( 7 );
            grafo.agregarArco( 3, 4, an );            
        }
        catch( VerticeYaExisteException e )
        {
            // Esto no deber��a suceder
            fail( );
        }
        catch( VerticeNoExisteException e )
        {
            // Esto no deber��a suceder
            fail( );
        }
        catch( ArcoYaExisteException e )
        {
            // Esto no deber��a suceder
            fail( );
        }
    }
    
    /**
     * Crea un grafo no conexo con 7 v�rtices y 11 arcos
     */
    public void setupEscenario8()
    {
     // Crear el grafo vac�o
        grafo = new GrafoNoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

        // Crear los v�rtices y los arcos
        try
        {
            VerticeNumerico vn = new VerticeNumerico( 0,0,0 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 1,1,1 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 2,2,2 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 3,3,3 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 4 ,4,4);
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 5 ,5,5);
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 6 ,6,6);
            grafo.agregarVertice( vn );

            ArcoNumerico an = new ArcoNumerico( 7 );
            grafo.agregarArco( 0, 1, an );
            
            an = new ArcoNumerico( 8 );
            grafo.agregarArco( 1, 2, an );
            
            an = new ArcoNumerico( 5 );
            grafo.agregarArco( 0, 3, an );
            
            an = new ArcoNumerico( 5 );
            grafo.agregarArco( 2, 4, an );
            
            an = new ArcoNumerico( 9 );
            grafo.agregarArco( 3, 1, an );
            
            an = new ArcoNumerico( 7 );
            grafo.agregarArco( 1, 4, an );
            
            an = new ArcoNumerico( 15 );
            grafo.agregarArco( 3, 4, an );
            
            an = new ArcoNumerico( 6 );
            grafo.agregarArco( 3, 5, an );
            
            an = new ArcoNumerico( 8 );
            grafo.agregarArco( 5, 4, an );
            
            an = new ArcoNumerico( 11 );
            grafo.agregarArco( 5, 6, an );
            
            an = new ArcoNumerico( 9 );
            grafo.agregarArco( 4, 6, an );
        }
        catch( VerticeYaExisteException e )
        {
            // Esto no deber��a suceder
            fail( );
        }
        catch( VerticeNoExisteException e )
        {
            // Esto no deber��a suceder
            fail( );
        }
        catch( ArcoYaExisteException e )
        {
            // Esto no deber��a suceder
            fail( );
        }
    }

    // -----------------------------------------------------------------
    // Pruebas para m�todos modificadores
    // -----------------------------------------------------------------

    /**
     * Pruebas de casos normales para el m�todo <code>agregarVertice</code>
     */
    public void testAgregarVertice( )
    {
        // Crear el grafo vacio
        setupEscenario1( );

        // Casos de agregar en un grafo vacio
        int nVerticesPrueba = 200;
        try
        {
            for( int i = 0; i < nVerticesPrueba; i++ )
            {
                VerticeNumerico vn = new VerticeNumerico( i,i,i );
                grafo.agregarVertice( vn );
                assertEquals( "El v�rtice en el grafo no corresponde al agregado", vn.darId( ), grafo.darVertice( i ).darId( ) );
                assertEquals( "El v�rtice en el grafo no corresponde al agregado", vn, grafo.darVertice( i ) );
            }
        }
        catch( VerticeYaExisteException e )
        {
            fail( "El v�rtice " + e.darIdentificador( ) + " agregado no existia. Debio ser agregado existosamente." );
            e.printStackTrace( );
        }
        catch( VerticeNoExisteException e )
        {
            fail( "El v�rtice " + e.darIdentificador( ) + " no fue agregado exitosamente" );
            e.printStackTrace( );
        }
    }

    /**
     * Pruebas de casos erroneos para el m�todo <code>agregarVertice</code>
     */
    public void testAgregarVerticeError( )
    {
        setupEscenario2( );

        // Ingresar un vertice repetido
        try
        {
            VerticeNumerico vn = new VerticeNumerico( 0,0,0 );
            grafo.agregarVertice( vn );
            fail( "El v�rtice ingresado ya existe" );
        }
        catch( VerticeYaExisteException e )
        {
            // Este es el comportamiento esperado
        }

        // Ingresar un vertice repetido
        try
        {
            VerticeNumerico vn = new VerticeNumerico( 2,2,2 );
            grafo.agregarVertice( vn );
            fail( "El v�rtice ingresado ya existe" );
        }
        catch( VerticeYaExisteException e )
        {
            // Este es el comportamiento esperado
        }
    }



    /**
     * Pruebas de casos normales para el m�todo <code>agregarArco</code>
     */

    /**
     * Pruebas de casos erroneos para el m�todo <code>agregarArco</code>
     */
    public void testAgregarArcoError( )
    {
        // Crear un grafo con 5 vertices en forma de lista encadenada
        setupEscenario4( );

        // Ingresar un arco entre vertices inexistentes
        try
        {
            grafo.agregarArco( 1, 6, new ArcoNumerico( 7 ) );
            fail( "Uno de los v�rtices conectados por el arco no existe" );
        }
        catch( VerticeNoExisteException e )
        {
            // Comportamiento esperado
        }
        catch( ArcoYaExisteException e )
        {
            fail( "Uno de los v�rtices conectados por el arco no existe" );
        }
        try
        {
            grafo.agregarArco( 6, 1, new ArcoNumerico( 7 ) );
            fail( "Uno de los v�rtices conectados por el arco no existe" );
        }
        catch( VerticeNoExisteException e )
        {
            // Comportamiento esperado
        }
        catch( ArcoYaExisteException e )
        {
            fail( "Uno de los v�rtices conectados por el arco no existe" );
        }
        try
        {
            grafo.agregarArco( 6, 6, new ArcoNumerico( 7 ) );
            fail( "Uno de los v�rtices conectados por el arco no existe" );
        }
        catch( VerticeNoExisteException e )
        {
            // Comportamiento esperado
        }
        catch( ArcoYaExisteException e )
        {
            fail( "Uno de los v�rtices conectados por el arco no existe" );
        }

        // Ingresar un arco repetido
        try
        {
            grafo.agregarArco( 1, 2, new ArcoNumerico( 7 ) );
            fail( "Ya existe un arco entre esos vertices" );
        }
        catch( VerticeNoExisteException e )
        {
            fail( "Ya existe un arco entre esos vertices" );
        }
        catch( ArcoYaExisteException e )
        {
            // Comportamiento esperado
        }
    }

    /**
     * Pruebas de casos normales para el m�todo <code>eliminarArco</code>
     */
   
    
    /**
     * Pruebas de casos normales para el m�todo <code>kruskal</code>
     */
   

}
