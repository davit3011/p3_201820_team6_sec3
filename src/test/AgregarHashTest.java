package test;

import org.junit.Test;

import model.data_structures.HashTableLinearProbing;
import model.data_structures.HashTableSeparateChaining;

public class AgregarHashTest 
{

	@Test
	public void cargaDatosOrdenadosLinearProbing()
	{
		int contador=0;
		String fufurufa="Holiwi";
		HashTableLinearProbing<String, Integer> hash = new HashTableLinearProbing<>();
		for(int i =0; i<1000000;i++)
		{
			fufurufa="pto"+contador;
			hash.put(fufurufa, contador);
			contador++;
		}
		System.out.println(hash.size());

	}
	@Test
	public void cargaDatosOrdenadosLinearProbingTresCuartos()
	{
		int contador=0;
		String fufurufa="Holiwi";
		HashTableLinearProbing<String, Integer> hash = new HashTableLinearProbing<>();
		for(int i =0; i<1000000;i++)
		{
			fufurufa="pto"+contador;
			hash.putTresCuartos(fufurufa, contador);
			contador++;
		}
		System.out.println(hash.size());

	}
	@Test
	public void cargaDatosOrdenadosLinearProbingUnTercio()
	{
		int contador=0;
		String fufurufa="Holiwi";
		HashTableLinearProbing<String, Integer> hash = new HashTableLinearProbing<>();
		for(int i =0; i<1000000;i++)
		{
			fufurufa="pto"+contador;
			hash.putUnTercio(fufurufa, contador);
			contador++;
		}
		System.out.println(hash.size());

	}
	@Test
	public void cargaDatosOrdenadosSeparateChaining()
	{
		int contador=0;
		String fufurufa="Holiwi"+contador;
		HashTableSeparateChaining<String, Integer> hash = new HashTableSeparateChaining<>();
		for(int i=0; i<1000000;i++)
		{
			fufurufa="pto"+contador;
			hash.put(fufurufa, contador);
			contador++;
		}
		System.out.println(hash.size());

	}
	@Test
	public void cargaDatosDesOrdenadosSeparateChaining()
	{
		int random = (int )(Math.random() * 1000000 + 1);
		int contador=0;
		String fufurufa="Holiwi"+random;
		HashTableSeparateChaining<String, Integer> hash = new HashTableSeparateChaining<>();
		for(int i=0; i<1000000;i++)
		{
			fufurufa="pto"+contador;
			contador++;

			hash.put(fufurufa, random);
		}
		System.out.println(hash.size());

	}
	@Test
	public void cargaDatosDesOrdenadosLinearProbing()
	{
		int random = (int )(Math.random() * 1000000 + 1);
		int contador=0;
		String fufurufa="Holiwi"+random;
		HashTableLinearProbing<String, Integer> hash = new HashTableLinearProbing<>();
		for(int i =0; i<1000000;i++)
		{
			fufurufa="pto"+contador;
			contador++;
			hash.put(fufurufa, random);
		}
		System.out.println(hash.size());
	}
	@Test
	public void cargaDatosDesOrdenadosLinearProbingTresCuartos()
	{
		int random = (int )(Math.random() * 1000000 + 1);
		int contador=0;
		String fufurufa="Holiwi"+random;
		HashTableLinearProbing<String, Integer> hash = new HashTableLinearProbing<>();
		for(int i =0; i<1000000;i++)
		{
			fufurufa="pto"+contador;
			contador++;
			hash.putTresCuartos(fufurufa, random);
		}
		System.out.println(hash.size());
	}
	@Test
	public void cargaDatosDesOrdenadosLinearProbingUnTercio()
	{
		int random = (int )(Math.random() * 1000000 + 1);
		int contador=0;
		String fufurufa="Holiwi"+random;
		HashTableLinearProbing<String, Integer> hash = new HashTableLinearProbing<>();
		for(int i =0; i<1000000;i++)
		{
			fufurufa="pto"+contador;
			contador++;
			hash.putUnTercio(fufurufa, random);
		}
		System.out.println(hash.size());
	}
}
