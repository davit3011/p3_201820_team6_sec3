package test;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import com.google.gson.Gson;
import com.opencsv.CSVReader;

import API.IManager;
import model.data_structures.ArcoNumerico;
import model.data_structures.DoubleLinkedList;
import model.data_structures.HashTableSeparateChaining;
import model.data_structures.RedBlackTree;
import model.data_structures.VerticeEstacion;
import model.data_structures.VerticeNumerico;
import model.data_structures.Lista.Lista;
import model.data_structures.grafo.ArcoNoExisteException;
import model.data_structures.grafo.ArcoYaExisteException;
import model.data_structures.grafo.IArco;
import model.data_structures.grafo.IVertice;
import model.data_structures.grafo.VerticeNoExisteException;
import model.data_structures.grafo.VerticeYaExisteException;
import model.data_structures.grafo.grafoDirigido.GrafoDirigido;
import model.data_structures.grafo.grafoNoDirigido.GrafoNoDirigido;
import model.data_structures.iterador.Iterador;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;
import model.vo.VerticeStation;
import junit.framework.TestCase;

/**
 * Clase utilizada para verificar el funcionamiento de la clase GrafoDirigido.
 * @see GrafoDirigido
 */
public class GrafoDirigidoTest extends TestCase
{
    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------	

    /**
     * Grafo sobre el que se van a hacer las pruebas.
     */
    private GrafoDirigido<Integer, VerticeNumerico, ArcoNumerico> grafo;

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------    
    
    /**
     * Crea un grafo vacio
     */
    public void setupEscenario1( )
    {
        // Crear al grafo vacio
        grafo = new GrafoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );
    }

    /**
     * Crea un grafo con 5 vertices y 0 arcos
     */
    public void setupEscenario2( )
    {
        // Crear el grafo vacio
        grafo = new GrafoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

        // Crear los vertices
        try
        {
            VerticeNumerico vn = new VerticeNumerico( 0,0,0 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 1,1,1 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 2,2,2 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 3,3,3 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 4 ,4,4);
            grafo.agregarVertice( vn );
        }
        catch( VerticeYaExisteException e )
        {
            // Esto no debería suceder
            fail( );
        }
    }

    /**
     * Crea un grafo con 5 v�rtices en forma de lista encadenada
     */
    public void setupEscenario3( )
    {
        // Crear el grafo vacio
        grafo = new GrafoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

        // Crear los v�rtices
        try
        {
            VerticeNumerico vn = new VerticeNumerico( 0,0,0 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 1,1,1 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 2,2,2 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 3,3,3 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 4,4,4 );
            grafo.agregarVertice( vn );
        }
        catch( VerticeYaExisteException e )
        {
            // Esto no deber�a suceder
            fail( );
        }

        // Agregar los v�rtices
        try
        {
            ArcoNumerico an = new ArcoNumerico( 1 );
            grafo.agregarArco( 0, 1, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 1, 2, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 2, 3, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 3, 4, an );
        }
        catch( VerticeNoExisteException e )
        {
            // Esto no deber��a
            fail( );
        }
        catch( ArcoYaExisteException e )
        {
            // Esto no deber��a
            fail( );
        }
    }

    /**
     * Crea un grafo con 10 vertices y 15 arcos
     */
    public void setupEscenario4( )
    {
        // Crear el grafo vacio
        grafo = new GrafoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

        // Crear los vertices
        try
        {
            VerticeNumerico vn = new VerticeNumerico( 1,1,1 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 2,2,2 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 3,3,3 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 4,4,4 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 5,5,5 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 6,6,6 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 7,7,7 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 8,8,8 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 9,9,9 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 10,10,10 );
            grafo.agregarVertice( vn );
        }
        catch( VerticeYaExisteException e )
        {
            // Esto no debería suceder
            fail( );
        }

        // Agregar los vertices
        try
        {
            ArcoNumerico an = new ArcoNumerico( 7 );
            grafo.agregarArco( 1, 3, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 2, 3, an );
            an = new ArcoNumerico( 9 );
            grafo.agregarArco( 3, 8, an );
            an = new ArcoNumerico( 8 );
            grafo.agregarArco( 3, 7, an );
            an = new ArcoNumerico( 6 );
            grafo.agregarArco( 4, 1, an );
            an = new ArcoNumerico( 6 );
            grafo.agregarArco( 4, 6, an );
            an = new ArcoNumerico( 3 );
            grafo.agregarArco( 5, 4, an );
            an = new ArcoNumerico( 3 );
            grafo.agregarArco( 6, 2, an );
            an = new ArcoNumerico( 5 );
            grafo.agregarArco( 6, 9, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 7, 5, an );
            an = new ArcoNumerico( 3 );
            grafo.agregarArco( 7, 6, an );
            an = new ArcoNumerico( 15 );
            grafo.agregarArco( 8, 4, an );
            an = new ArcoNumerico( 2 );
            grafo.agregarArco( 8, 10, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 9, 8, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 10, 7, an );
        }
        catch( VerticeNoExisteException e )
        {
            // Esto no debería
            fail( );
        }
        catch( ArcoYaExisteException e )
        {
            // Esto no debería
            fail( );
        }
    }

    /**
     * Crea un grafo con 10 vertices y 20 arcos que tiene un camino hamiltoniano
     */
    public void setupEscenario5( )
    {
        // Crear el grafo vacio
        grafo = new GrafoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

        // Crear los vertices
        try
        {
            VerticeNumerico vn = new VerticeNumerico( 1,1,1 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 2,2,2 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 3,3,3 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 4,4,4 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 5,5,5 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 6,6,6 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 7,7,7 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 8,8,8 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 9,9,9 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 10,10,10 );
            grafo.agregarVertice( vn );
        }
        catch( VerticeYaExisteException e )
        {
            // Esto no debería suceder
            fail( );
        }

        // Agregar los vertices
        try
        {
            ArcoNumerico an = new ArcoNumerico( 1 );
            grafo.agregarArco( 1, 2, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 1, 3, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 1, 4, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 2, 3, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 2, 6, an );
            an = new ArcoNumerico( 2 );
            grafo.agregarArco( 2, 9, an );
            an = new ArcoNumerico( 3 );
            grafo.agregarArco( 3, 8, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 4, 6, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 5, 1, an );
            an = new ArcoNumerico( 8 );
            grafo.agregarArco( 5, 4, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 5, 7, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 6, 7, an );
            an = new ArcoNumerico( 2 );
            grafo.agregarArco( 7, 3, an );
            an = new ArcoNumerico( 5 );
            grafo.agregarArco( 8, 4, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 9, 6, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 9, 8, an );
            an = new ArcoNumerico( 3 );
            grafo.agregarArco( 9, 10, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 10, 8, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 10, 7, an );
            an = new ArcoNumerico( 5 );
            grafo.agregarArco( 10, 5, an );
        }
        catch( VerticeNoExisteException e )
        {
            // Esto no debería
            fail( );
        }
        catch( ArcoYaExisteException e )
        {
            // Esto no debería
            fail( );
        }
    }

    /**
     * Crea un grafo igual al escenario 5 y le agrega un arco de peso 1 entre los nodos 8 y 1
     */
    public void setupEscenario6( )
    {
        // Crear el grafo vacio
        grafo = new GrafoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

        // Crear los vertices
        try
        {
            VerticeNumerico vn = new VerticeNumerico( 1,1,1 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 2,2,2 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 3,3,3 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 4,4,4 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 5,5,5 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 6,6,6 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 7,7,7 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 8,8,8 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 9,9,9 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 10,10,10 );
            grafo.agregarVertice( vn );
        }
        catch( VerticeYaExisteException e )
        {
            // Esto no deber��a suceder
            fail( );
        }

        // Agregar los v�rtices
        try
        {
            ArcoNumerico an = new ArcoNumerico( 1 );
            grafo.agregarArco( 1, 2, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 1, 3, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 1, 4, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 2, 3, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 2, 6, an );
            an = new ArcoNumerico( 2 );
            grafo.agregarArco( 2, 9, an );
            an = new ArcoNumerico( 3 );
            grafo.agregarArco( 3, 8, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 4, 6, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 5, 1, an );
            an = new ArcoNumerico( 8 );
            grafo.agregarArco( 5, 4, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 5, 7, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 6, 7, an );
            an = new ArcoNumerico( 2 );
            grafo.agregarArco( 7, 3, an );
            an = new ArcoNumerico( 5 );
            grafo.agregarArco( 8, 4, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 8, 1, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 9, 6, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 9, 8, an );
            an = new ArcoNumerico( 3 );
            grafo.agregarArco( 9, 10, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 10, 8, an );
            an = new ArcoNumerico( 4 );
            grafo.agregarArco( 10, 7, an );
            an = new ArcoNumerico( 5 );
            grafo.agregarArco( 10, 5, an );
        }
        catch( VerticeNoExisteException e )
        {
            // Esto no deber��a
            fail( );
        }
        catch( ArcoYaExisteException e )
        {
            // Esto no deber�a
            fail( );
        }
    }

    /**
     * Crea un grafo de 10 v�rtices y 17 vertices que tiene un camino hamiltoniano
     */
    public void setupEscenario7( )
    {
        // Crear el grafo vacio
        grafo = new GrafoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

        // Crear los v�rtices
        try
        {
            VerticeNumerico vn = new VerticeNumerico( 1,1,1 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 2,2,2 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 3,3,3 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 4,4,4 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 5,5,5 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 6,6,6 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 7,7,7 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 8,8,8 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 9,9,9 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 10,10,10 );
            grafo.agregarVertice( vn );
        }
        catch( VerticeYaExisteException e )
        {
            // Esto no debería suceder
            fail( );
        }

        // Agregar los vertices
        try
        {
            ArcoNumerico an = new ArcoNumerico( 1 );
            grafo.agregarArco( 1, 2, an );
            an = new ArcoNumerico( 2 );
            grafo.agregarArco( 2, 9, an );
            an = new ArcoNumerico( 10 );
            grafo.agregarArco( 3, 2, an );
            an = new ArcoNumerico( 3 );
            grafo.agregarArco( 3, 8, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 4, 6, an );
            an = new ArcoNumerico( 8 );
            grafo.agregarArco( 5, 4, an );
            an = new ArcoNumerico( 10 );
            grafo.agregarArco( 6, 2, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 6, 7, an );
            an = new ArcoNumerico( 10 );
            grafo.agregarArco( 6, 9, an );
            an = new ArcoNumerico( 2 );
            grafo.agregarArco( 7, 3, an );
            an = new ArcoNumerico( 10 );
            grafo.agregarArco( 7, 5, an );
            an = new ArcoNumerico( 10 );
            grafo.agregarArco( 7, 10, an );
            an = new ArcoNumerico( 5 );
            grafo.agregarArco( 8, 4, an );
            an = new ArcoNumerico( 10 );
            grafo.agregarArco( 8, 9, an );
            an = new ArcoNumerico( 10 );
            grafo.agregarArco( 8, 10, an );
            an = new ArcoNumerico( 3 );
            grafo.agregarArco( 9, 10, an );
            an = new ArcoNumerico( 5 );
            grafo.agregarArco( 10, 5, an );

        }
        catch( VerticeNoExisteException e )
        {
            // Esto no debería
            fail( );
        }
        catch( ArcoYaExisteException e )
        {
            // Esto no debería
            fail( );
        }
    }

    /**
     * Crea un grafo de 5 v�rtices y 8 arcos.
     */
    public void setupEscenario8( )
    {
        // Crear el grafo vacio
        grafo = new GrafoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

        // Crear los vertices
        try
        {
            VerticeNumerico vn = new VerticeNumerico( 1,1,1 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 2,2,2 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 3,3,3 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 4,4,4 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 5,5,5 );
            grafo.agregarVertice( vn );
        }
        catch( VerticeYaExisteException e )
        {
            // Esto no debería suceder
            fail( );
        }

        // Agregar los vertices
        try
        {
            ArcoNumerico an = new ArcoNumerico( 1 );
            grafo.agregarArco( 5, 4, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 4, 1, an );
            an = new ArcoNumerico( 2 );
            grafo.agregarArco( 1, 2, an );
            an = new ArcoNumerico( 3 );
            grafo.agregarArco( 2, 3, an );
            an = new ArcoNumerico( 5 );
            grafo.agregarArco( 3, 4, an );
            an = new ArcoNumerico( 8 );
            grafo.agregarArco( 4, 2, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 2, 5, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 5, 3, an );
        }
        catch( VerticeNoExisteException e )
        {
            // Esto no debería
            fail( );
        }
        catch( ArcoYaExisteException e )
        {
            // Esto no debería
            fail( );
        }
    }

    public void setupEscenario9( )
    {
        // Crear el grafo vacio
        grafo = new GrafoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

        // Crear los vertices
        try
        {
            VerticeNumerico vn = new VerticeNumerico( 1,1,1 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 2,2,2 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 3,3,3 );
            grafo.agregarVertice( vn );
            vn = new VerticeNumerico( 4,4,4 );
            grafo.agregarVertice( vn );
        }
        catch( VerticeYaExisteException e )
        {
            // Esto no debería suceder
            fail( );
        }

        // Agregar los arcos
        try
        {
            ArcoNumerico an = new ArcoNumerico( 1 );
            grafo.agregarArco( 1, 2, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 2, 1, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 2, 3, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 1, 4, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 1, 3, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 2, 4, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 3, 4, an );
            an = new ArcoNumerico( 1 );
            grafo.agregarArco( 4, 3, an );

        }
        catch( VerticeNoExisteException e )
        {
            // Esto no debería
            fail( );
        }
        catch( ArcoYaExisteException e )
        {
            // Esto no debería
            fail( );
        }
    }

    /**
     * Pruebas de casos normales para el método <code>agregarVertice</code>
     */
    public void testAgregarVertice( )
    {
        // Crear el grafo vacio
        setupEscenario1( );

        // Casos de agregar en un grafo vacio
        VerticeNumerico vn = new VerticeNumerico( 1,1,1 );
        try
        {
            grafo.agregarVertice( vn );
            assertEquals( "El grafo deber�a tener 1 vertice", 1, grafo.darObjetosVertices( ).size( ) );
            assertEquals( "El vertice en el grafo no corresponde al agregado", vn.darId( ), grafo.darVertice( 1 ).darId( ) );
            assertEquals( "El vertice en el grafo no corresponde al agregado", vn, grafo.darVertice( 1 ) );
        }
        catch( VerticeYaExisteException e )
        {
            fail( "El vertice agregado no existe" );
            e.printStackTrace( );
        }
        catch( VerticeNoExisteException e )
        {
            fail( "El vertice buscado si existe" );
            e.printStackTrace( );
        }

        // Casos de agregar en un grafo no vacio
        VerticeNumerico vn2 = new VerticeNumerico( 2,2,2 );
        try
        {
            grafo.agregarVertice( vn2 );
            assertEquals( "El grafo debería tener 2 vertice", 2, grafo.darObjetosVertices( ).size( ) );
            assertEquals( "No se encuentran vertices agregados anteriormente", vn.darId( ), grafo.darVertice( 1 ).darId( ) );
            assertEquals( "No se encuentran vertices agregados anteriormente", vn, grafo.darVertice( 1 ) );
            assertEquals( "El vertice en el grafo no corresponde al agregado", vn2.darId( ), grafo.darVertice( 2 ).darId( ) );
            assertEquals( "El vertice en el grafo no corresponde al agregado", vn2, grafo.darVertice( 2 ) );
        }
        catch( VerticeYaExisteException e )
        {
            fail( "El vertice agregado no existe" );
            e.printStackTrace( );
        }
        catch( VerticeNoExisteException e )
        {
            fail( "El vertice buscado si existe" );
            e.printStackTrace( );
        }
    }

    /**
     * Pruebas de casos erroneos para el método <code>agregarVertice</code>
     */
    public void testAgregarVerticeError( )
    {
        setupEscenario2( );

        // Ingresar un vertice repetido
        try
        {
            VerticeNumerico vn = new VerticeNumerico( 0,0,0 );
            grafo.agregarVertice( vn );
            fail( "El vertice ingresado ya existe" );
        }
        catch( VerticeYaExisteException e )
        {
            // Este es el comportamiento esperado
        }
    }

    /**
     * Pruebas de casos normales para el método <code>agregarArco</code>
     */
    public void testAgregarArco( )
    {
        // Crear un grafo sin arcos
        setupEscenario2( );

        try
        {
            ArcoNumerico an = new ArcoNumerico( 5 );
            grafo.agregarArco( 0, 1, an );
            assertNotNull( "El arco no quedo bien agregado", grafo.darArco( 0, 1 ) );
            assertEquals( "Deberia haber solo un arco en el grafo", 1, grafo.darNArcos( ) );
            assertEquals( "El arco agregado no corresponde con el arco devuelto por el grafo", an, grafo.darArco( 0, 1 ) );

            an = new ArcoNumerico( 10 );
            grafo.agregarArco( 1, 2, an );
            assertNotNull( "El arco no quedo bien agregado", grafo.darArco( 1, 2 ) );
            assertEquals( "Deberia haber solo un arco en el grafo", 2, grafo.darNArcos( ) );
            assertEquals( "El arco agregado no corresponde con el arco devuelto por el grafo", an, grafo.darArco( 1, 2 ) );

            an = new ArcoNumerico( 5 );
            grafo.agregarArco( 2, 3, an );
            assertNotNull( "El arco no quedo bien agregado", grafo.darArco( 2, 3 ) );
            assertEquals( "Deberia haber solo un arco en el grafo", 3, grafo.darNArcos( ) );
            assertEquals( "El arco agregado no corresponde con el arco devuelto por el grafo", an, grafo.darArco( 2, 3 ) );

            an = new ArcoNumerico( 10 );
            grafo.agregarArco( 3, 4, an );
            assertNotNull( "El arco no quedo bien agregado", grafo.darArco( 3, 4 ) );
            assertEquals( "Deberia haber solo un arco en el grafo", 4, grafo.darNArcos( ) );
            assertEquals( "El arco agregado no corresponde con el arco devuelto por el grafo", an, grafo.darArco( 3, 4 ) );
        }
        catch( VerticeNoExisteException e )
        {
            // Esto no debería suceder;
            fail( );
        }
        catch( ArcoYaExisteException e )
        {
            fail( "El arco ingresado no existe" );
            e.printStackTrace( );
        }
        catch( ArcoNoExisteException e )
        {
            fail( e.getMessage( ) );
            e.printStackTrace( );
        }

    }

    /**
     * Pruebas de casos erroneos para el método <code>agregarArco</code>
     */
    public void testAgregarArcoError( )
    {
        setupEscenario2( );

        // Agregar un arco a un nodo no existente
        ArcoNumerico an = new ArcoNumerico( 1 );
        try
        {
            grafo.agregarArco( 0, 5, an );
            fail( "El destino del arco no existe" );
        }
        catch( VerticeNoExisteException e )
        {
            // Este es el comportamiento esperado
        }
        catch( ArcoYaExisteException e )
        {
            fail( "El arco agregado no existe" );
            e.printStackTrace( );
        }

        // Agregar un arco a un nodo no existente
        an = new ArcoNumerico( 1 );
        try
        {
            grafo.agregarArco( 5, 0, an );
        }
        catch( VerticeNoExisteException e )
        {
            // Este es el comportamiento esperado
        }
        catch( ArcoYaExisteException e )
        {
            fail( "El arco agregado no existe" );
            e.printStackTrace( );
        }

        // Agregar un arco repetido
        an = new ArcoNumerico( 1 );
        ArcoNumerico an2 = new ArcoNumerico( 2 );
        try
        {
            grafo.agregarArco( 0, 1, an );
            grafo.agregarArco( 0, 1, an2 );
            fail( "El arco agregado está repetido" );
        }
        catch( VerticeNoExisteException e )
        {
            fail( "Los vertices si existe" );
            e.printStackTrace( );
        }
        catch( ArcoYaExisteException e )
        {
            // Este es el comportamiento esperado
        }

    }

    /**
     * Pruebas de casos normales para el método <code>hayCamino</code>
     */
    public void testHayCamino( )
    {
        setupEscenario3( );

        try
        {
            assertTrue( "Algoritmo no valido para encontrar camino", grafo.hayCamino( 0, 1 ) );
            assertTrue( "Algoritmo no valido para encontrar camino", grafo.hayCamino( 0, 2 ) );
            assertTrue( "Algoritmo no valido para encontrar camino", grafo.hayCamino( 0, 3 ) );
            assertTrue( "Algoritmo no valido para encontrar camino", grafo.hayCamino( 0, 4 ) );
            assertFalse( "Algoritmo no valido para encontrar camino", grafo.hayCamino( 1, 0 ) );
            assertFalse( "Algoritmo no valido para encontrar camino", grafo.hayCamino( 4, 0 ) );
        }
        catch( VerticeNoExisteException e )
        {
            // esto no debería suceder
            fail( );
        }
    }

    /**
     * Pruebas de casos normales para el método <code>darCaminoMasCorto</code>
     */
  

    /**
     * Pruebas de casos normales para el método <code>hayCiclo</code>
     */
    public void testHayCiclo( )
    {
        setupEscenario3( );
        try
        {
            assertFalse( "En este grafo no hay ciclos", grafo.hayCiclo( 0 ) );
        }
        catch( VerticeNoExisteException e )
        {
            // Esto no debería suceder
            fail( );
        }

        setupEscenario4( );
        try
        {
            assertTrue( "En este vertice si hay ciclo", grafo.hayCiclo( 4 ) );
            assertTrue( "En este vertice si hay ciclo", grafo.hayCiclo( 6 ) );
            assertTrue( "En este vertice si hay ciclo", grafo.hayCiclo( 2 ) );
            assertTrue( "En este vertice si hay ciclo", grafo.hayCiclo( 8 ) );
        }
        catch( VerticeNoExisteException e )
        {
            // Esto no debería suceder
            fail( );
        }
    }

    /**
     * Pruebas para el método <code>hayCaminoHamiltoniano</code>
     */
    public void testHayCaminoHamiltoniano( )
    {
        setupEscenario4( );
        assertFalse( "En este grafo no hay camino hamiltoniano", grafo.hayCaminoHamilton( ) );
        setupEscenario2( );
        assertFalse( "En este grafo no hay camino hamiltoniano", grafo.hayCaminoHamilton( ) );

        setupEscenario3( );
        assertTrue( "En este grafo si hay camino hamiltoniano", grafo.hayCaminoHamilton( ) );
        setupEscenario5( );
        assertTrue( "En este grafo si hay camino hamiltoniano", grafo.hayCaminoHamilton( ) );
        setupEscenario7( );
        assertTrue( "En este grafo si hay camino hamiltoniano", grafo.hayCaminoHamilton( ) );
    }

    /**
     * Pruebas para el método <code>darCaminoHamiltoniano</code>s
     */
  

    /**
     * Pruebas para el método <code>hayCaminoEuler</code>
     */
   
    /**
     * Pruebas para el método <code>darCaminoEuler</code>
     */
   



    /**
     * Pruebas para el método <code>darPeso</code>
     */
    public void testDarPeso( )
    {
        setupEscenario1( );
        assertEquals( "Algoritmo no valido para calcular el peso del grafo", 0, grafo.darPeso( ) );
        setupEscenario2( );
        assertEquals( "Algoritmo no valido para calcular el peso del grafo", 0, grafo.darPeso( ) );
        setupEscenario3( );
        assertEquals( "Algoritmo no valido para calcular el peso del grafo", 4, grafo.darPeso( ) );
        setupEscenario4( );
        assertEquals( "Algoritmo no valido para calcular el peso del grafo", 77, grafo.darPeso( ) );
        setupEscenario5( );
        assertEquals( "Algoritmo no valido para calcular el peso del grafo", 68, grafo.darPeso( ) );
        setupEscenario6( );
        assertEquals( "Algoritmo no valido para calcular el peso del grafo", 69, grafo.darPeso( ) );
        setupEscenario7( );
        assertEquals( "Algoritmo no valido para calcular el peso del grafo", 101, grafo.darPeso( ) );
        setupEscenario8( );
        assertEquals( "Algoritmo no valido para calcular el peso del grafo", 22, grafo.darPeso( ) );

    }

    /**
     * Pruebas para el método <code>esCompleto</code>
     */
    public void testEsCompleto( )
    {
        setupEscenario2( );
        assertFalse( "Este grafo no es completo", grafo.esCompleto( ) );
        setupEscenario3( );
        assertFalse( "Este grafo no es completo", grafo.esCompleto( ) );
        setupEscenario4( );
        assertFalse( "Este grafo no es completo", grafo.esCompleto( ) );
        setupEscenario5( );
        assertFalse( "Este grafo no es completo", grafo.esCompleto( ) );
        setupEscenario6( );
        assertFalse( "Este grafo no es completo", grafo.esCompleto( ) );
        setupEscenario7( );
        assertFalse( "Este grafo no es completo", grafo.esCompleto( ) );
        setupEscenario8( );
        assertFalse( "Este grafo no es completo", grafo.esCompleto( ) );

        setupEscenario1( );
        assertTrue( "Este grafo es completo", grafo.esCompleto( ) );
        setupEscenario9( );
        assertTrue( "Este grafo es completo", grafo.esCompleto( ) );
    }

    /**
     * Pruebas para el m�todo <code>darExcentricidad</code>
     */
    public void testDarExcentricidad( )
    {
        try
        {
            setupEscenario2( );
            assertEquals( "Algoritmo no valido para calcular excentricidad", GrafoDirigido.INFINITO, grafo.darExcentricidad( 1 ) );
            assertEquals( "Algoritmo no valido para calcular excentricidad", GrafoDirigido.INFINITO, grafo.darExcentricidad( 2 ) );

            setupEscenario3( );
            assertEquals( "Algoritmo no valido para calcular excentricidad", 4, grafo.darExcentricidad( 0 ) );
            assertEquals( "Algoritmo no valido para calcular excentricidad", GrafoDirigido.INFINITO, grafo.darExcentricidad( 2 ) );
            assertEquals( "Algoritmo no valido para calcular excentricidad", GrafoDirigido.INFINITO, grafo.darExcentricidad( 4 ) );

        }
        catch( VerticeNoExisteException e )
        {
            // Esto no deber�a suceder
            fail( );
        }
    }

}
