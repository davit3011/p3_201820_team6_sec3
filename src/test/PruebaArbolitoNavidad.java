package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.RedBlackTree;

public class PruebaArbolitoNavidad {

	RedBlackTree<String, Integer> arbol = new RedBlackTree<>();
	@Test
	public void test() {
		assertTrue(arbol.isEmpty());
		arbol.put("numero", 10);
		arbol.put("numero1", 8);
		arbol.put("numero2", 7);
		arbol.put("numero3", 20);
		arbol.put("numero4", 83);
		arbol.put("numero5", 1);
		arbol.put("numero6", 100);
		assertTrue(arbol.floor("numero2.9").equals("numero2"));
		assertTrue(arbol.ceiling("numero3.1").equals("numero4"));
		assertTrue(arbol.contains("numero6"));
		assertTrue(arbol.select(3).equals("numero3"));
		assertTrue(arbol.select(4).equals("numero4"));
		assertFalse(arbol.isEmpty());
		assertTrue(arbol.max().equals("numero6"));
		assertTrue(arbol.min().equals("numero"));
		arbol.deleteMax();
		arbol.deleteMin();
		assertTrue(arbol.max().equals("numero5"));
		assertTrue(arbol.min().equals("numero1"));
		assertNotNull(arbol.get("numero3"));
		arbol.delete("numero3");
		assertNull(arbol.get("numero3"));
	}
}