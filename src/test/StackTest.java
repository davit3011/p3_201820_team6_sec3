package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.IStack;
import model.data_structures.Stack;

public class StackTest<T> {

	 @Test
	    public void pushTwoObjectsToEmtyStackCheckThatSizeIsTwo() {
	        pushObjectsInOrder(OBJECT_A, OBJECT_B);
	        assertEquals(2, stack.getSize());
	    }
	    @Test
	    public void pushTwoObjectToEmptyStackAndPopTheSameObjectsInReversedOrder() {
	        pushObjectsInOrder(OBJECT_A, OBJECT_B);
	        assertEquals(OBJECT_B, stack.pop());
	        assertEquals(OBJECT_A, stack.pop());
	    }
	    @Test(expected=IllegalArgumentException.class)
	    public void stackDoesntAcceptNullAndThrowExcpetion() {
	        stack.push(null);
	    }
	    @Before
	    public void setUp() throws Exception {
	        stack = new Stack();
	    }
	    private void pushObjectsInOrder(Object... objects) {
	        for (Object object : objects) {
	            stack.push(object);
	        }
	    }
	    private static final Object OBJECT_A = new Object();
	    private static final Object OBJECT_B = new Object();
	    private Stack<Object> stack;
}
