package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.IQueue;
import model.data_structures.Queue;

public class QueueTest {
	IQueue<String> queueTest;

    @Before
    public void setUp() throws Exception {
        queueTest = new Queue<>();
        // queueTest = new GenericArrayQueue<>();
    }

    @Test
    public void testAdd() {
        queueTest.enqueue("5");
        queueTest.enqueue("4");
        queueTest.enqueue("3");
        queueTest.enqueue("2");
        queueTest.enqueue("1");

        assertEquals("5", queueTest.dequeue());
        assertEquals("4", queueTest.dequeue());
        assertEquals("3", queueTest.dequeue());
        assertEquals("2", queueTest.dequeue());
        assertEquals("1", queueTest.dequeue());
    }

    

    @Test
    public void testRemove() {
        queueTest.enqueue("5");
        queueTest.enqueue("4");
        queueTest.enqueue("3");
        queueTest.enqueue("2");
        queueTest.enqueue("1");    

        assertEquals("5", queueTest.dequeue());
        assertEquals("4", queueTest.dequeue());
        assertEquals("3", queueTest.dequeue());
        assertEquals("2", queueTest.dequeue());
        assertEquals("1", queueTest.dequeue());
    }



   

   
}
