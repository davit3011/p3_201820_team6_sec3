package model.data_structures;

import java.util.EmptyStackException;
import java.util.Iterator;

public class Stack<G> implements IStack
{
	    protected Nodo<G> top;
	    protected int size;      

	    public Stack()
	    {
	        top = null;
	        size = 0;
	    }
	    public int getSize() {
	        return size;
	    }
   @Override
	    public boolean isEmpty() {
	        if(top == null)
	            return true;
	        return false;
	    }

	    public G top() throws EmptyStackException {
	        if(isEmpty())
	            throw new EmptyStackException();
	        return top.darElemento();
	    }
	    @Override
	    public G pop() throws EmptyStackException {
	        if(isEmpty())
	            throw new EmptyStackException();
	        G temp = top.darElemento();
	        top = top.darSiguiente();
	        size--;
	        return temp;
	    }
		@Override
		public Iterator iterator() {
			// TODO Auto-generated method stub
			return new Iterador<>(top);
		}
		
		public void pushe(G elemento) {
	        Nodo<G> oldfirst = top;
	        top = new Nodo<G>(elemento);
	        top.cambiarSiguiente(oldfirst);
	        size++;
	 		
		}
		@Override
		public void push(Object elemento) {
			// TODO Auto-generated method stub
			
		}
		
		
		
	}





