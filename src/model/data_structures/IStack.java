package model.data_structures;

import java.util.Iterator;

public interface IStack<G> extends Iterable<G>{
	
    
	public int getSize();
    
	public boolean isEmpty();

    public Iterator<G> iterator();
    
    public G pop();

	void push(G elemento);

	
	
}
