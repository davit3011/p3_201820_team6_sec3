/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id: GrafoNoDirigido.java,v 1.4 2008/12/11 16:33:20 alf-mora Exp $
 * Universidad de los Andes (Bogot� - Colombia)
 * Departamento de Ingenier�a de Sistemas y Computaci�n 
 * Licenciado bajo el esquema Academic Free License version 2.1 
 *
 * Proyecto Cupi2 (http://cupi2.uniandes.edu.co)
 * Framework: Cupi2Collections
 * Autor: Juan Erasmo G�mez - Feb 29, 2008
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package model.data_structures.grafo.grafoNoDirigido;

import java.io.Serializable;
import java.util.Iterator;

import model.data_structures.Lista.Lista;
import model.data_structures.grafo.ArcoNoExisteException;
import model.data_structures.grafo.ArcoYaExisteException;
import model.data_structures.grafo.ElCamino;
import model.data_structures.grafo.IArco;
import model.data_structures.grafo.IVertice;
import model.data_structures.grafo.VerticeNoExisteException;
import model.data_structures.grafo.VerticeYaExisteException;
import model.data_structures.*;
public class GrafoNoDirigido<K, V extends IVertice<K>, A extends IArco> implements Serializable
{

	private static final long serialVersionUID = 1L;	
	private HashTableSeparateChaining<A, Lista<IVertice<K>>> matrizAdyacencia;
	private HashTableSeparateChaining<K, V> vertices;
	public GrafoNoDirigido( )
	{
		matrizAdyacencia = new HashTableSeparateChaining<A, Lista<IVertice<K>>>(700000);
		vertices = new HashTableSeparateChaining<>(700000);
	}
	public void agregarVertice( V elemento ) throws VerticeYaExisteException
	{
		vertices.put( elemento.darId() ,   elemento );

	}
	public HashTableSeparateChaining<A, Lista<IVertice<K>>> darMatriz(){
		return matrizAdyacencia;
	}
	public void agregarArco( K idVerticeOrigen, K idVerticeDestino, A infoArco ) throws VerticeNoExisteException, ArcoYaExisteException
	{
		Lista<IVertice<K>> agregar = new Lista<>();
		IVertice<K> origen = vertices.get(idVerticeOrigen);
		IVertice<K> destino = vertices.get(idVerticeDestino);
		agregar.agregar(origen);
		agregar.agregar(destino);

		matrizAdyacencia.put(infoArco , agregar );
	}
	public V darVertice( K idVertice ) throws VerticeNoExisteException
	{
		return vertices.get(idVertice);
	}
	public boolean existeVertice( K idVertice )
	{

		return vertices.contains(idVertice);
	}
	public HashTableSeparateChaining<K, V> darVertices( )
	{

		return vertices;
	}
	public int darCantidadVertices( )
	{
		return vertices.size();
	}
	public boolean existeArco( A id) throws VerticeNoExisteException
	{

		return matrizAdyacencia.contains(id);
	}
	public A darArco( IVertice<K> v1, IVertice<K> v2) throws VerticeNoExisteException, ArcoNoExisteException
	{
		A rta=null;
		while(matrizAdyacencia.keys().iterator().hasNext())
		{
			A actual=matrizAdyacencia.keys().iterator().next();
			Lista <IVertice<K>> lista=matrizAdyacencia.get(actual);
			if(lista.contiene(v1)&&lista.contiene(v2))
			{
				rta=actual;
			}
		}
		return rta;
	}

	public int darNArcos( )
	{
		return matrizAdyacencia.size();
	}


	public HashTableSeparateChaining<A, Lista<IVertice<K>>> darArcos( )
	{
		return matrizAdyacencia;
	}

	public double darPeso( )
	{
		double rta=0;
		Iterator<A> iter = matrizAdyacencia.keys().iterator();
		while(iter.hasNext())
		{
			A arco = iter.next();
			rta += arco.darPesoODistancia();

		}
		return rta;
	}
	public Lista<IVertice<K>> adj(IVertice<K> vertice)
	{

		Lista<IVertice<K>> rta = new Lista<>();
		Queue<A> keys = matrizAdyacencia.keys();
		for (A x : keys) 
		{
			Lista <IVertice<K>>lista =matrizAdyacencia.get(x);
			if(lista.contiene(vertice))
			{
				for( int j=0; j<lista.darLongitud(); j++)
				{
					if(!rta.contiene(lista.darElemento(j)))
					{
						rta.agregar(lista.darElemento(j));	
					}
				}
			}
		}

		return rta;
	}
}
