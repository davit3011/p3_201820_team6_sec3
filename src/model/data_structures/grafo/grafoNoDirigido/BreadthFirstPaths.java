package model.data_structures.grafo.grafoNoDirigido;

import model.data_structures.Queue;
import model.data_structures.Stack;
import model.data_structures.grafo.IArco;
import model.data_structures.grafo.IVertice;
import model.data_structures.grafo.VerticeNoExisteException;
import model.data_structures.iterador.Iterador;
/**
 *  The {@code BreadthFirstPaths} class represents a data type for finding
 *  shortest paths (number of edges) from a source vertex <em>s</em>
 *  (or a set of source vertices)
 *  to every other vertex in an undirected graph.
 *  <p>
 *  This implementation uses breadth-first search.
 *  The constructor takes time proportional to <em>V</em> + <em>E</em>,
 *  where <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
 *  Each call to {@link #distTo(int)} and {@link #hasPathTo(int)} takes constant time;
 *  each call to {@link #pathTo(int)} takes time proportional to the length
 *  of the path.
 *  It uses extra space (not including the graph) proportional to <em>V</em>.
 *  <p>
 *  For additional documentation,
 *  see <a href="https://algs4.cs.princeton.edu/41graph">Section 4.1</a>   
 *  of <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class BreadthFirstPaths {
	private static final int INFINITY = Integer.MAX_VALUE;
	private boolean[] marked;  // marked[v] = is there an s-v path
	private int[] edgeTo;      // edgeTo[v] = previous edge on shortest s-v path
	private int[] distTo;      // distTo[v] = number of edges shortest s-v path
	private Stack<Integer> pathe;

	/**
	 * Computes the shortest path between the source vertex {@code s}
	 * and every other vertex in the graph {@code G}.
	 * @param G the graph
	 * @param s the source vertex
	 * @throws VerticeNoExisteException 
	 * @throws IllegalArgumentException unless {@code 0 <= s < V}
	 */
	public BreadthFirstPaths(GrafoNoDirigido<Integer, IVertice<Integer>, IArco> G, int s) throws VerticeNoExisteException {
		marked = new boolean[G.darCantidadVertices()];
		distTo = new int[G.darCantidadVertices()];
		edgeTo = new int[G.darCantidadVertices()];
		bfs(G, s);
	}

	/**
	 * Computes the shortest path between any one of the source vertices in {@code sources}
	 * and every other vertex in graph {@code G}.
	 * @param G the graph
	 * @param sources the source vertices
	 * @throws VerticeNoExisteException 
	 * @throws IllegalArgumentException unless {@code 0 <= s < V} for each vertex
	 *         {@code s} in {@code sources}
	 */
	public BreadthFirstPaths(GrafoNoDirigido<Integer, ?, ?> G, Iterable<Integer> sources) throws VerticeNoExisteException {
		marked = new boolean[G.darCantidadVertices()];
		distTo = new int[G.darCantidadVertices()];
		edgeTo = new int[G.darCantidadVertices()];
		for (int v = 0; v < G.darCantidadVertices(); v++)
			distTo[v] = INFINITY;
		bfs(G, sources);
	}


	private void bfs(GrafoNoDirigido<Integer, IVertice<Integer>, IArco> G, int s) throws VerticeNoExisteException {
		Queue<Integer> q = new Queue<Integer>();
		for (int v = 0; v < G.darCantidadVertices(); v++) 
			distTo[v] = INFINITY;
		distTo[s] = 0;
		marked[s] = true;
		q.enqueue(s);

		while (!q.isEmpty()) {
			
			int v = q.dequeue();
			IVertice<Integer> h=   G.darVertice(v);
			Iterador<IVertice<Integer>> iterador=G.adj(h).darIterador();
			
			while(iterador.haySiguiente())
			{
			
				
				
				IVertice<Integer> w = iterador.darSiguiente();
				if (!marked[w.darId()]) 
				{
					edgeTo[w.darId()] = v;
					distTo[w. darId()] = distTo[v] + 1;
					marked[w.darId()] = true;
					q.enqueue(w.darId());
				}
			}

		}
	}


	private void bfs(GrafoNoDirigido<Integer, ?, ?> G, Iterable<Integer> sources) throws VerticeNoExisteException {
		Queue<Integer> q = new Queue<Integer>();
		for (int s : sources) {
			marked[s] = true;
			distTo[s] = 0;
			q.enqueue(s);
		}
		while (!q.isEmpty()) {
			int v = q.dequeue();
			IVertice<Integer> h = G.darVertice(v);
			while(G.adj(h).darIterador().haySiguiente())
			{
				IVertice<Integer> w =  (IVertice<Integer>) G.adj(h).darIterador().darSiguiente();
				if (!marked[w.darId()]) {
					edgeTo[w.darId()] = v;
					distTo[w.darId()] = distTo[v] + 1;
					marked[w.darId()] = true;
					q.enqueue(w.darId());
				}
			}
		}
	}

	/**
	 * Is there a path between the source vertex {@code s} (or sources) and vertex {@code v}?
	 * @param v the vertex
	 * @return {@code true} if there is a path, and {@code false} otherwise
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public boolean hasPathTo(int v) {
		return marked[v];
	}

	/**
	 * Returns the number of edges in a shortest path between the source vertex {@code s}
	 * (or sources) and vertex {@code v}?
	 * @param v the vertex
	 * @return the number of edges in a shortest path
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public int distTo(int v) {
		return distTo[v];
	}

	/**
	 * Returns a shortest path between the source vertex {@code s} (or sources)
	 * and {@code v}, or {@code null} if no such path.
	 * @param  v the vertex
	 * @return the sequence of vertices on a shortest path, as an Iterable
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public Stack<Integer> pathTo(int v) {
		System.out.println(hasPathTo(v)+"path toooo");
		if (hasPathTo(v)==false) return null;
		pathe = new Stack<Integer>();
		int x;
		for (x = v; distTo[x] != 0; x = edgeTo[x]) {
			pathe.pushe(x);
		}
		pathe.pushe(x);
		return pathe;
	}
	// check optimality conditions for single source


	// check that v = edgeTo[w] satisfies distTo[w] = distTo[v] + 1
	// provided v is reachable from s




}
/******************************************************************************
 *  Copyright 2002-2018, Robert Sedgewick and Kevin Wayne.
 *
 *  This file is part of algs4.jar, which accompanies the textbook
 *
 *      Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 *
 *
 *  algs4.jar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  algs4.jar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with algs4.jar.  If not, see http://www.gnu.org/licenses.
 ******************************************************************************/

