
package model.data_structures.grafo.dijkstra;

import java.util.*;

import model.data_structures.grafo.IArco;
import model.data_structures.grafo.IVertice;
import model.data_structures.grafo.grafoDirigido.Arco;
import model.data_structures.grafo.grafoDirigido.Vertice;
import model.data_structures.iterador.Iterador;
import model.data_structures.iterador.IteradorException;
import model.data_structures.iterador.IteradorSimple;



public class CaminosMinimos<K, V extends IVertice<K>, A extends IArco>
{

	private Vertice<K, V, A> origen;


	private HashMap<K, NodoDijkstra<K, V, A>> nodos;


	private ArrayList<Vertice<K, V, A>> sinMarcar;


	public CaminosMinimos( Vertice<K, V, A> pOrigen, Collection<Vertice<K, V, A>> pVertices )
	{
		origen = pOrigen;
		nodos = new HashMap<K, NodoDijkstra<K, V, A>>( );
		sinMarcar = new ArrayList<Vertice<K, V, A>>( );
		NodoDijkstra<K, V, A> nodoInicial = null;
		for( Vertice<K, V, A> vert : pVertices )
		{
			NodoDijkstra<K, V, A> nodo = new NodoDijkstra<K, V, A>( vert );
			nodos.put( vert.darId( ), nodo );
			if( !vert.darId( ).equals( origen.darId( ) ) )
				sinMarcar.add( vert );
			else
				nodoInicial = nodo;
		}
		for( Arco<K, V, A> arco : origen.darSucesores( ) )
		{
			asignarCosto( arco.darVerticeDestino( ).darId( ), (int) arco.darPeso( ), nodoInicial );
		}
		asignarCosto( origen.darId( ), 0, null );
		origen.marcar( );
	}


	public void asignarCosto( K idVertice, int costo, NodoDijkstra<K, V, A> anterior )
	{
		NodoDijkstra<K, V, A> nodo = nodos.get( idVertice );
		nodo.asignarCostoMinimo( costo, anterior );
	}


	public Vertice<K, V, A> darSiguienteVertice( )
	{
		Vertice<K, V, A> menorV = null;
		NodoDijkstra<K, V, A> menorN = null;

		// Recorrer los vertices sin marcar
		for( Vertice<K, V, A> vert : sinMarcar )
		{
			NodoDijkstra<K, V, A> nodo = nodos.get( vert.darId( ) );
			// Si no he inicializado el menor vertice, y el nodo en el que estoy tiene un costo minimo != indefinido
			if( menorV == null && nodo.darCostoMinimo( ) != NodoDijkstra.INDEFINIDO )
			{
				menorV = vert;
				menorN = nodo;
			}

			else if( menorN != null && nodo.darCostoMinimo( ) < menorN.darCostoMinimo( ) )
			{
				menorV = vert;
				menorN = nodo;
			}
		}

		if( menorV != null )
		{
			sinMarcar.remove( menorV );
			menorV.marcar( );
		}
		return menorV;
	}

	public void recalcularCaminosEspeciales( Vertice<K, V, A> nuevoVert )
	{
		NodoDijkstra<K, V, A> nuevoNodo = nodos.get( nuevoVert.darId( ) );
		int costoANuevoVert = nuevoNodo.darCostoMinimo( );
		for( Arco<K, V, A> arco : nuevoVert.darSucesores( ) )
		{
			Vertice<K, V, A> vert = arco.darVerticeDestino( );
			if( !vert.marcado( ) )
			{
				NodoDijkstra<K, V, A> nodo = nodos.get( vert.darId( ) );
				if( nodo.darCostoMinimo( ) > costoANuevoVert + arco.darPeso( ) )
					nodo.asignarCostoMinimo( (int) (costoANuevoVert + arco.darPeso( )), nuevoNodo );
			}
		}
	}

	public void recalcularCaminosEspecialesToll( Vertice<K, V, A> nuevoVert )
	{
		NodoDijkstra<K, V, A> nuevoNodo = nodos.get( nuevoVert.darId( ) );
		int costoANuevoVert = nuevoNodo.darCostoMinimo( );
		for( Arco<K, V, A> arco : nuevoVert.darSucesores( ) )
		{
			Vertice<K, V, A> vert = arco.darVerticeDestino( );
			if( !vert.marcado( ) )
			{
				NodoDijkstra<K, V, A> nodo = nodos.get( vert.darId( ) );
				if( nodo.darCostoMinimo( ) > costoANuevoVert + arco.darPeso( ) )
					nodo.asignarCostoMinimo( (int) (costoANuevoVert + arco.darPeso( )), nuevoNodo );
			}
		}
	}


	public int darCostoCamino( Vertice<K, V, A> vertice )
	{
		NodoDijkstra<K, V, A> nodo = nodos.get( vertice.darId( ) );
		int costo = nodo.darCostoMinimo( );
		return costo == NodoDijkstra.INDEFINIDO ? -1 : costo;
	}

	Iterador<Vertice<K, V, A>> darCaminoMinimo( Vertice<K, V, A> vertice )
	{
		NodoDijkstra<K, V, A> nodo = nodos.get( vertice.darId( ) );
		IteradorSimple<Vertice<K, V, A>> itera = new IteradorSimple<Vertice<K, V, A>>( nodos.size( ) );
		try
		{
			while( nodo != null )
			{
				itera.insertar( nodo.darVertice( ) );
				nodo = nodo.darPredecesor( );
			}
		}
		catch( IteradorException e )
		{
			e.printStackTrace( );
		}
		return itera;
	}
}
