
package model.data_structures.grafo.dijkstra;

import java.io.Serializable;

import model.data_structures.grafo.IArco;
import model.data_structures.grafo.IVertice;
import model.data_structures.grafo.grafoDirigido.Vertice;
import model.vo.VerticeStation;



public class NodoDijkstra<K, V extends IVertice<K>, A extends IArco> implements Serializable
{
   

 
	private static final long serialVersionUID = 1L;

	
	public final static int INDEFINIDO = Integer.MAX_VALUE;

   
    private int costoMinimo;

   
    private Vertice<K, V, A> vertice;


    private NodoDijkstra<K, V, A> predecesor;

    public NodoDijkstra( Vertice<K, V, A> vert )
    {
        costoMinimo = INDEFINIDO;
        vertice = vert;
        predecesor = null;
    }

   
    public int darCostoMinimo( )
    {
        return costoMinimo;
    }

    
    public void asignarCostoMinimo( int costo, NodoDijkstra<K, V, A> anterior )
    {
        costoMinimo = costo;
        predecesor = anterior;
    }

   
    public Vertice<K, V, A> darVertice( )
    {
        return vertice;
    }

   
    public NodoDijkstra<K, V, A> darPredecesor( )
    {
        return predecesor;
    }
}
