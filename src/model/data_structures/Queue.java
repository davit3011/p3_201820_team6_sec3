package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue<E> implements IQueue<E>  {

	private Nodo<E> first, last;
	private int size;
	public Queue()
	{
		last =null;
		first=null;
		size=0;
	}

	@Override
	public void enqueue(E element) {
		Nodo <E> newNode = new Nodo<> (element);
		if(size == 0)
		{
			first = newNode;
			last = newNode;
		}
		else
		{
			Nodo<E> oldLast = last;
			oldLast.cambiarSiguiente(newNode);
			last = newNode;
		}
		size++;
	}

	



	@Override
	public E dequeue() throws NoSuchElementException {
		E elemento = first.darElemento();
		if(size<2)
		{
			first = null;
			last = null;   	
			size --;
		}
		else
		{
			Nodo<E> nuevoPrimero = first.darSiguiente();
			first= nuevoPrimero;
			first.cambiarAnterior(null);
			size--;
		}
		return elemento;
	}

	

	
	




	@Override
	public Iterator<E> iterator() {
		return new Iterador<E>(first);
		
	}


	@Override
	public boolean isEmpty() {
		
		return (size==0)? true: false;
	}
	


	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}




}

