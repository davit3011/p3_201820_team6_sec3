package model.data_structures;

import java.util.Iterator;

public interface IList<T> extends Iterable<T>{
Integer getSize();
	
	public void addFirst(T item);
	public void append(T item);
	public void addAtPosition(T item, int i);
	public void removeFirst();
	public void remove (int pos);
	public Nodo<T> get (int pos);
	public int size();
	public Nodo<T> next();
	public Nodo<T> previous();
	public boolean isEmpty();
	public Nodo<T> darPrimero();
	public Nodo<T> darUltimo();

	public Iterator<T> iterator();
}

