package model.data_structures;

	/**
	* Representa un vértice que contiene un dato numérico.
	*/
	public class VerticeEstacion implements model.data_structures.grafo.IVertice<Integer>
	{
	//-----------------------------------------------------------------
	//Constantes
	//-----------------------------------------------------------------

	/**
	* Constante para la serialización 
	*/
	private static final long serialVersionUID = 1L;

	//-----------------------------------------------------------------
	//Atributos
	//----------------------------------------------------------------- 

	/**
	* Dato contenido por el vértice.
	*/
	private int identificador;

	private double longitud;

	private double latitud;

	private String tipo;

	//-----------------------------------------------------------------
	//Constructores
	//----------------------------------------------------------------- 

	/**
	* Constructor de la clase.
	* @param valor Dato contenido por el vértice.
	*/
	public VerticeEstacion( int pId, double longi, double lati, String pTipo)
	{
		this.identificador = pId;
		this.longitud = longi;
		this.latitud = lati;
		this.tipo = pTipo;
	}

	//-----------------------------------------------------------------
	//Métodos
	//----------------------------------------------------------------- 

	/*
	* (non-Javadoc)
	* @see uniandes.cupi2.collections.grafo.IVertice#darId()
	*/


	public double darLongitud()
	{
		return longitud;
	}

	public double darLatitud()
	{
		return latitud;
	}

	@Override
	public Integer darId() {
		// TODO Auto-generated method stub
		return identificador;
	}

	public String darTipo() {
		// TODO Auto-generated method stub
		return tipo;
	}

	@Override
	public double calcularCantidadViajesPorDia() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void marcar() {
		// TODO Auto-generated method stub
		
	}

}
