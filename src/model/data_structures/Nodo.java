package model.data_structures;

import java.util.Comparator;

/**
 * Nodo
 * @author Juan Antonio Restrepo
 *
 * @param <T>
 */
public class Nodo<T>{

	//------------------------------
	//ATRIBUTOS
	//------------------------------

	/**
	 * Atributo que modela el nodo anterior
	 */
	private Nodo<T> anterior;

	/**
	 * Atributo que modela el nodo siguiente
	 */
	private Nodo<T> siguiente;

	/**
	 * Atributo que modela el elemento contenido en el nodo.
	 */
	protected T elemento;

	//-----------------------------
	//CONSTRUCTORES
	//------------------------------

	/**
	 * Constructor para un nodo doblemente encadenado.
	 * @param pElemento que va a ser contenido en el nodo. 
	 */
	public Nodo(T pElemento) 
	{
		elemento = pElemento;
		anterior = null;
		siguiente = null;
	}

	//------------------------------
	//METODOS
	//------------------------------
	
	/**
	 * Metodo que cambia el nodo siguiente
	 * @param pSiguiente el nodo que se va a cambiar por el siguiente. 
	 */
	public void cambiarSiguiente(Nodo<T> pSiguiente)
	{
		siguiente = pSiguiente;
	}

	/**
	 * Metodo que cambia el nodo anterior
	 * @param pAnterior el nodeo que se va a cambiar por el anterior. 
	 */
	public void cambiarAnterior(Nodo<T> pAnterior)
	{
		anterior = pAnterior;
	}

	/**
	 * Metodo que retorna el siguiente elemento de este nodo
	 * @return el nodo siguiente a este
	 */
	public Nodo<T> darSiguiente()
	{
		return siguiente;
	}

	/**
	 * Metodo que retorna el anterior elemento de este nodo. 
	 * @return el nodo anterior a este.
	 */
	public Nodo<T> darAnterior()
	{
		return anterior;
	}

	/**
	 * Metodo que retorna el elemento contenido dentro de este nodo.
	 * @return el elemento contenido en este nodo. 
	 */
	public T darElemento()
	{
		return elemento;
	}

	/**
	 * Metodo que cambia el elemento contenido en este nodo. 
	 * @param pNuevo el elemento que se quiere cambiar en este nodo. 
	 */
	public void cambiarElemento(T pNuevo)
	{
		elemento =  pNuevo;
	}

}
