package model.data_structures;

import java.util.Iterator;

import javax.print.attribute.standard.NumberOfDocuments;



public class DoubleLinkedList<T extends Comparable<T>> implements IDoublyLinkedList<T>{

	private int numeroItems;
	private Nodo<T> primero;
	private Nodo<T> ultimo;
	public DoubleLinkedList()
	{
		primero=null;
		numeroItems=0;

	}

	public DoubleLinkedList(T pElemento)
	{
		if(pElemento == null)
		{
			throw new NullPointerException("Elemento nulo");
		}
		primero = new Nodo<T>(pElemento);
		numeroItems=1;
	}

	public Nodo<T> darPrimero()
	{
		return primero;
	}
	public boolean estaVacia() {
		return numeroItems == 0;
	}


	public T obtener(int indice) {
		Nodo<T> actual =primero;
		int i =0;
		while(actual!= null && i!= indice)
		{
			actual=actual.darSiguiente();
			i++;
		}
		return actual.darElemento();
	}
	public int size()
	{
		return numeroItems;
	}

	@Override
	public boolean add(T t) 
	{
		boolean sePudo = false;
		Nodo<T> actual = primero;
		Nodo<T> nuevo = new Nodo<T>(t);
		if(t != null)
		{
			if(primero== null)
			{
				primero= nuevo;
				numeroItems++;
				sePudo=true;
			}else if (ultimo==null){
				ultimo=nuevo;
				nuevo.cambiarAnterior(primero);
				primero.cambiarSiguiente(nuevo);
				numeroItems++;
			}else{
				Nodo<T> local = ultimo;
				local.cambiarSiguiente(nuevo);
				ultimo=nuevo;
				ultimo.cambiarAnterior(local);
				numeroItems++;
			}
		}
		return sePudo;
	}



	@Override
	public Iterator<T> iterator() {
		return new Iterador<T>(primero); 
	}
	

}
