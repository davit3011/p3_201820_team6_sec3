package model.data_structures;

public interface IMaxHeapCP<T> extends Iterable<T> 
{

	void add(T value);
	T[] getHeap();
	T remove();
	boolean remove(T value);
	T poll();
	boolean isEmpty();
	T peek();
	int length();
	
	
	
	
}
