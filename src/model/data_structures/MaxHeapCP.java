package model.data_structures;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;

public class MaxHeapCP<T extends Comparable<T>> implements IMaxHeapCP<T>
{
	private T[] heap;
	private int length;
	private boolean min;
	private Comparable[] pq; // heap-ordered complete binary tree
	private int N = 0;

	public MaxHeapCP(int capacidadInicial)
	{
		heap = (T[]) new Comparable[capacidadInicial+1];
		length = 0;
		min = true;
		pq =  new Comparable[capacidadInicial+1];
	}

	
	public MaxHeapCP(T[] array, boolean min, int capacidadInicial)
	{
		heap = (T[]) new Comparable[capacidadInicial+1];
		length = 0;
		this.min = min;

		for (T each : array)
		{
			add(each);
		}
	}
	public Comparable delMax()
	{
		Comparable max = pq[1]; // Retrieve max key from top.
		exch(1, N--); // Exchange with last item.
		pq[N+1] = null; // Avoid loitering.
		sink(1); // Restore heap property.
		return max;
	}
	private void sink(int k)
	{
		while (2*k <= N)
		{
			int j = 2*k;
			if (j < N && less(j, j+1)) j++;
			if (!less(k, j)) break;
			exch(k, j);
			k = j;
		}
	}
	


	public MaxHeapCP(boolean min, int capacidadInicial)
	{
		heap = (T[]) new Comparable[capacidadInicial+1];
		length = 0;
		this.min = min;

	}
	private void exch(int i, int j)
	{ Comparable t = pq[i]; pq[i] = pq[j]; pq[j] = t; }

	@Override
	public T[] getHeap()
	{
		return Arrays.copyOfRange(heap, 1, length + 1);
	}

   @Override
	public void add(T value)
	{
		// resize if needed
		if (this.length >= heap.length - 1)
		{
			heap = this.resize();
		}

		length++;
		heap[length] = value;
		bubbleUp();
	}

	@Override
	public T remove()
	{
		T result = peek();

		swap(1, length);
		heap[length] = null;
		length--;

		bubbleDown();

		return result;
	}

	@Override
	public boolean remove(T value)
	{
		for (int i = 0; i < heap.length; i++)
		{
			if (value.equals(heap[i]))
			{
				System.out.println(i);
				swap(i, length);
				heap[length] = null;
				length--;
				//bubbleUp();
				bubbleDown();
				return true;
			}
		}
		return false;
	}

	@Override
	public T poll()
	{
		if (isEmpty()) return null;

		T result = peek();

		swap(1, length);
		heap[length] = null;
		length--;

		bubbleDown();

		return result;
	}

	@Override
	public boolean isEmpty()
	{
		return length == 0;
	}

	@Override
	public T peek()
	{
		if (isEmpty()) throw new IllegalStateException();
		return heap[1];
	}

	@Override
	public int length()
	{
		return length;
	}

	private T[] resize()
	{
		// add 10 to array capacity
		return Arrays.copyOf(heap, heap.length + 2*length);
	}
	
	private void bubbleUp()
	{
		int index = length;
		
			while (hasParent(index) && (parent(index).compareTo(heap[index]) < 0))
			{
				swap(index, parentIndex(index));
				index = parentIndex(index);
			}	

		}
	

	
	private void bubbleDown()
	{
		int index = 1;
		
			while (hasLeftChild(index))
			{
				// find larger of child values
				int larger = leftIndex(index);
				if (hasRightChild(index) && heap[leftIndex(index)].compareTo(heap[rightIndex(index)]) < 0)
				{
					larger = rightIndex(index);
				}
				if (heap[index].compareTo(heap[larger]) < 0)
				{
					swap(index, larger);
				}
				else break;

				index = larger;
			}				

		}
	

	
	private boolean hasParent(int i)
	{
		return i > 1;
	}

	
	private int leftIndex(int i)
	{
		return i * 2;
	}

	
	private int rightIndex(int i)
	{
		return i * 2 + 1;
	}


	private boolean hasLeftChild(int i)
	{
		return leftIndex(i) <= length;
	}


	private boolean hasRightChild(int i)
	{
		return rightIndex(i) <= length;
	}


	private int parentIndex(int i)
	{
		return i / 2;
	}


	private T parent(int i)
	{
		return heap[parentIndex(i)];
	}

	
	private void swap(int index1, int index2)
	{
		T temp = heap[index1];
		heap[index1] = heap[index2];
		heap[index2] = temp;
	}

	
	@Override
	public String toString()
	{
		String retval = "";
		for (T each : heap)
		{
			if (each != null) retval += each + " : ";
		}
		return retval + "\n";
	
	}
	public Comparable[] getPQ()
	{
		return pq;
	}


	 private void resize(int capacity) {
	        T[] temp = (T[]) new Object[capacity];
	        for (int i = 1; i <= N; i++) {
	            temp[i] = (T) pq[i];
	        }
	        pq = temp;
	    }

	 
	public T max() {
	      if (!isEmpty()) 
	      {
	        T max = (T) pq[1];
	        exch(1, N--);
	        sink(1);
	        pq[N+1] = null;   
	        if ((N> 0) && (N == (pq.length - 1) / 4)) resize(pq.length / 2);
	        return max;
	      }
	      return null;      
	}
	public int size()
	{ return N; }

	public void insert(T v)
	{
		pq[++N] = v;
		swim(N);
	}

	private void swim(int k)
	{
		while (k > 1 && less(k/2, k))
		{
			exch(k/2, k);
			k = k/2;
		}
	}

	private boolean less(int i, int j)
	{ return pq[i].compareTo(pq[j]) < 0; }


	/**
     * Rearranges the array in ascending order, using the natural order.
     * @param pq the array to be sorted
     */
    public static void sort(Comparable[] pq) {
        int n = pq.length;
        for (int k = n/2; k >= 1; k--)
            sink(pq, k, n);
        while (n > 1) {
            exch(pq, 1, n--);
            sink(pq, 1, n);
        }
    }

   /***************************************************************************
    * Helper functions to restore the heap invariant.
    ***************************************************************************/

    private static void sink(Comparable[] pq, int k, int n) {
        while (2*k <= n) {
            int j = 2*k;
            if (j < n && less(pq, j, j+1)) j++;
            System.out.println("k sink: " + k + "n sink: " + n);
            if (!less(pq, k, j)) break;
            exch(pq, k, j);
            k = j;
        }
    }

   /***************************************************************************
    * Helper functions for comparisons and swaps.
    * Indices are "off-by-one" to support 1-based indexing.
    ***************************************************************************/
    private static boolean less(Comparable[] pq, int i, int j) {
    	System.out.println("i: " + i + " j: " + j);
        return pq[i-1].compareTo(pq[j-1]) < 0;
    }

    private static void exch(Object[] pq, int i, int j) {
        Object swap = pq[i-1];
        pq[i-1] = pq[j-1];
        pq[j-1] = swap;
    }


	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}



}