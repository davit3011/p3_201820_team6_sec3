package model.data_structures;

import model.data_structures.grafo.IVertice;

/**
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* $Id: VerticeNumerico.java,v 1.2 2008/10/09 16:32:35 alf-mora Exp $
* Universidad de los Andes (Bogot� - Colombia)
* Departamento de Ingenier�a de Sistemas y Computaci�n 
* Licenciado bajo el esquema Academic Free License version 2.1 
*
* Proyecto Cupi2 (http://cupi2.uniandes.edu.co)
* Framework: Cupi2Collections
* Autor: Juan Erasmo G�mez - Abril 8, 2008
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/


/**
* Representa un v�rtice que contiene un dato num�rico.
*/
public class VerticeNumerico implements IVertice<Integer>, Comparable<VerticeNumerico>
{
// -----------------------------------------------------------------
// Constantes
// -----------------------------------------------------------------

/**
* Constante para la serializaci�n 
*/
private static final long serialVersionUID = 1L;

// -----------------------------------------------------------------
// Atributos
// ----------------------------------------------------------------- 

/**
* Dato contenido por el v�rtice.
*/
private Integer identificador;

private double longitud;

private double latitud;

// -----------------------------------------------------------------
// Constructores
// ----------------------------------------------------------------- 

/**
* Constructor de la clase.
* @param valor Dato contenido por el v�rtice.
*/
public VerticeNumerico( int pId, double longi, double lati)
{
	this.identificador = pId;
	this.longitud = longi;
	this.latitud = lati;
}

// -----------------------------------------------------------------
// M�todos
// ----------------------------------------------------------------- 

/*
* (non-Javadoc)
* @see uniandes.cupi2.collections.grafo.IVertice#darId()
*/
public Integer darId( )
{
	return identificador;
}

public double darLongitud()
{
	return longitud;
}

public double darLatitud()
{
	return latitud;
}

@Override
public int compareTo(VerticeNumerico o) {
	// TODO Auto-generated method stub
	return 0;
}

@Override
public double calcularCantidadViajesPorDia() {
	// TODO Auto-generated method stub
	return 0;
}

@Override
public void marcar() {
	// TODO Auto-generated method stub
	
}
}

