package model.data_structures;

import java.util.Iterator;

public class LinkedList<T> implements IList<T>, Iterable<T>
{
	private Nodo<T> list; 
	private int listSize;
	private Nodo<T> ultimo;

	//Tomado de presentaciones en clase

	public LinkedList()
	{
		list = null;
		ultimo = null;
	}

	public LinkedList(T item)
	{
		list = new Nodo<T> (item);
		ultimo = list;
		listSize = 1;
	}
	@Override
	public Integer getSize() {
		return listSize;
	}

	@Override
	public void addFirst(T item) {

		Nodo <T> newHead = new Nodo<T> (item); 
		if(list == null){
			list = newHead;
			ultimo = list;
		}
		else
		{
			newHead.cambiarSiguiente(list);
			list.cambiarAnterior(newHead);
			list = newHead;
		}
		listSize++;
	}

	@Override
	public void append(T item) {
		Nodo <T> newNodo = new Nodo<> (item); 
		if(list == null)
		{
			list = newNodo;
			ultimo = list;
		}
		else {
			ultimo.cambiarSiguiente(newNodo);
			newNodo.cambiarAnterior(ultimo);
			ultimo = newNodo;
		}
		listSize++;
	}

	@Override
	public void removeFirst() {
		list = list.darSiguiente();
		if(list != null)
			list.cambiarAnterior(null);
		listSize--;
	}

	@Override
	public void remove(int pos) {
		if(listSize > pos)
		{
			Nodo prev = get(pos).darAnterior();
			Nodo nex = get(pos).darSiguiente();
			if(prev == null){
				removeFirst();
				return;
			}
			if(nex != null)
			{
				nex.cambiarAnterior(prev);
			}
			if(prev != null)
			{
				prev.cambiarSiguiente(nex);
				if(prev.darSiguiente() == null)
					ultimo = prev;
			}
		}

	}

	@Override
	public Nodo<T> get(int pos) {
		int contador = 0;
		Nodo <T> actual = list; 
		while(actual.darSiguiente() != null && contador<pos && pos<=listSize) {
			actual = actual.darSiguiente();
			contador++;
		}
		return actual;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return listSize;
	}

	@Override
	public boolean isEmpty() {
		boolean a = false;
		if(list == null)
			a = true;

		return a;
	}

	@Override
	public void addAtPosition(T item, int i) {
		Nodo<T> nuevo = new Nodo<T>(item);
		Nodo<T> actual = get(i);
		if(actual == null)
		{
			append(item);
			return;
		}
		if(actual.darAnterior() != null)
		{
			nuevo.cambiarAnterior(actual.darAnterior());
			actual.darAnterior().cambiarSiguiente(nuevo);
		}
		else
			list = nuevo;

		actual.cambiarAnterior(nuevo);
		nuevo.cambiarSiguiente(actual);
		listSize++;
	}


	@Override
	public Nodo next() {
		return list.darSiguiente();
	}

	@Override
	public Nodo previous() {
		return list.darAnterior();
	}

	public Nodo darPrimero()
	{
		return list;
	}

	public Nodo darUltimo()
	{
		return ultimo;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>() 
		{
			Nodo<T> act= list;
			public boolean hasNext()
			{
				if (listSize == 0)
				return false;
				if (act == null)
					return true;
				return act.darSiguiente() != null;
			}
			public T next()
			{
				
				if (act == null)
					act = list;
				else
					act = act.darSiguiente();
				return act.darElemento();

			}
		};
	}
	}