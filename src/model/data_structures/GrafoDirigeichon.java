package model.data_structures;

public class GrafoDirigeichon <K  , V , A> {

	private int numV;

	private int numE;

	private HashTableLinearProbing< K , Integer > ht;

	private V infoV[];
	private LinkedList< ArcoG<K , A> > listaAdj[];

	private LinkedList<ArcoG<K, A>> listaArcoGs;

	private LinkedList<IVertice<K>> listaVertices;




	@SuppressWarnings("unchecked")
	public GrafoDirigeichon(int n){
		numV = 0;
		numE = 0;
		ht = new HashTableLinearProbing<>(3*n);
		infoV = (V[]) new Object[n];
		listaAdj = new LinkedList[n];
		listaArcoGs = new LinkedList<>();
		listaVertices = new LinkedList<>();
	}

	public int V(){
		return numV;
	}

	public int E(){
		return numE;
	}

	public void addVertex(K idV , V infoVertex){

		int pos = numV;
		ht.put(idV, pos);
		infoV[pos] = infoVertex;
		listaAdj[pos] = new LinkedList<>();
		numV++;
		IVertice<K> inf = (IVertice<K>)infoVertex;
		listaVertices.append(inf);
	}

	public void addEdge(K idVIni , K idVFin , A infoArc){
		int pos = ht.get(idVIni);

		ArcoG<K, A> arc = new ArcoG<K,A>(idVIni , idVFin , infoArc);
		listaAdj[pos].append(arc);
		int pos2 = ht.get(idVFin);
		listaArcoGs.append(arc);
		ArcoG<K, A> arc2 = new ArcoG<K, A>(idVFin , idVIni , infoArc);
		listaAdj[pos2].append(arc2);
		listaArcoGs.append(arc2);
		numE++;
		listaArcoGs.append(arc);
	}

	public void addEdgeD(K idVIni , K idVFin , A infoArc){
		int pos = ht.get(idVIni);
		ArcoG<K, A> arc = new ArcoG<K, A>(idVIni , idVFin , infoArc);
		listaAdj[pos].append(arc);
		//		int pos2 = ht.get(idVFin);
		//		ArcoG arc2 = new ArcoG(idVFin , idVIni , infoArc);
		//		listaAdj[pos2].append(arc2);
		numE++;
		//		listaArcoGs.append(arc);
	}

	public V getInfoVertex(K idVertex){
		int pos = ht.get(idVertex);
		return infoV[pos];
	}

	public void setinfoVertex(K idVertex , V infoVertex){
		int pos = ht.get(idVertex);
		infoV[pos] = infoVertex;
	}

	public A getInfoArc(K idVertexIni , K idVertexFin){
		LinkedList<ArcoG<K , A>> adj = listaAdj[ht.get(idVertexIni)];

		for(ArcoG<K , A> a : adj){
			if(a.getIdF().equals(idVertexFin)){
				return a.getInfoArc();

			}
		}

		return null;

	}

	public void setInfoArc(K idVertexIni , K idVertexFin , A infoArc){
		LinkedList<ArcoG<K , A>> adj = listaAdj[ht.get(idVertexIni)];

		for(ArcoG<K , A> a : adj){
			if(a.getIdF() == idVertexFin){
				a.setInfoArc(infoArc);
			}
		}
	}

	public LinkedList<ArcoG<K, A>> adj(K idVertex){
		LinkedList<ArcoG<K , A>> adj = listaAdj[ht.get(idVertex)];
		LinkedList<ArcoG<K, A>> idAdjs = new LinkedList<>();
		//		System.out.println("TAMAÃ‘O "+ adj.size());
		idAdjs.addFirst((ArcoG<K, A>)adj.darPrimero().darElemento());
		for(ArcoG<K , A> a : adj){
			idAdjs.append(a);
		}

		return idAdjs;
	}

	public LinkedList<ArcoG<K, A>> edges()
	{
		return listaArcoGs;
	}

	public LinkedList<IVertice<K>> vertices()
	{
		return listaVertices;
	}

	public GrafoDirigeichon<Integer,VerticeNumerico,Double> reverse(GrafoDirigeichon<Integer, VerticeNumerico, Double> G) {
		GrafoDirigeichon<Integer, VerticeNumerico, Double> reverse = new GrafoDirigeichon<>(numV);
//		LinkedList<VerticeNumerico> lis = G.vertices();
//		VerticeNumerico prim = (VerticeNumerico)listaVertices.darPrimero().getItem();
//		reverse.addVertex(prim.darId(), prim);
//		for (VerticeNumerico verticeNumerico : lis) {
//			reverse.addVertex(verticeNumerico.darId(), verticeNumerico);
//		}
//		
//		LinkedList<ArcoG<Integer, Double>> ArcoGs = G.edges();
//		ArcoG<Integer, Double> primero = (ArcoG<Integer, Double>)ArcoGs.darPrimero().getItem();
//		reverse.addEdgeD(primero.getIdF(), primero.getIdF(), primero.getInfoArc());
//		for (ArcoG<Integer, Double> ArcoG : ArcoGs) {
//			reverse.addEdgeD(ArcoG.getIdF(), ArcoG.getIdI(), ArcoG.getInfoArc());
//		}
		return reverse;
	}

/*	private void desmarcar( )
	{
		// Elimina todas las marcas presentes en los v�rtices del grafo
		for( IVertice<K> vertice : listaVertices )
		{
			vertice.desmarcar( );
		}
	}

	public LinkedList<Componente> contArcoGmponentesConexos( )
	{
		int compConexos = 0;

		LinkedList<Componente> rta = new LinkedList<>();

		desmarcar( );

		for( IVertice<K> vertice : listaVertices )
		{
			if( !vertice.estaMarcado() )
			{
				int componente1 = vertice.marcarAdyacentes();
				Componente componente = new Componente(componente1);
				rta.append(componente);
				System.out.println("com: " +componente1);
				compConexos++;
			}
		}
		return rta;
	}*/

}
