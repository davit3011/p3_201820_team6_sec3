package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.opencsv.CSVReader;
import com.teamdev.jxmaps.swing.MapView;

import API.IManager;
import model.data_structures.ArcoG;
import model.data_structures.ArcoNumerico;
import model.data_structures.DjikstraSP;
import model.data_structures.GrafoDirigeichon;
import model.data_structures.HashTableSeparateChaining;
import model.data_structures.LinkedList;
import model.data_structures.MaxHeapCP;
import model.data_structures.Queue;
import model.data_structures.VerticeNumerico;
import model.data_structures.Lista.Lista;
import model.data_structures.grafo.ArcoYaExisteException;
import model.data_structures.grafo.ElCamino;
import model.data_structures.grafo.IArco;
import model.data_structures.grafo.IVertice;
import model.data_structures.grafo.VerticeNoExisteException;
import model.data_structures.grafo.VerticeYaExisteException;
import model.data_structures.grafo.grafoDirigido.Arco;
import model.data_structures.grafo.grafoDirigido.GrafoDirigido;
import model.data_structures.grafo.grafoNoDirigido.BreadthFirstPaths;
import model.data_structures.grafo.grafoNoDirigido.GrafoNoDirigido;
import model.vo.ComponenteConexo;
import model.vo.Trip;
import model.vo.VerticeStation;

public class Manager extends MapView implements IManager  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Ruta del archivo de datos 2017-Q1
	public static final String TRIPS_Q1 = "data/Divvy_Trips_2017_Q1.csv";
	//Ruta archivo del JSON
	public final static String JSON ="./data/CDOT_Bike_Routes_2014_1216-transformed.json";

	//Ruta del archivo de trips 2017-Q2
	public static final String TRIPS_Q2 = "data/Divvy_Trips_2017_Q2.csv";

	//Ruta del archivo de trips 2017-Q3
	public static final String TRIPS_Q3 = "data/Divvy_Trips_2017_Q3.csv";

	//Ruta del archivo de trips 2017-Q4
	public static final String TRIPS_Q4 = "data/Divvy_Trips_2017_Q4.csv";

	//Ruta del archivo de stations 2017-Q1-Q2
	public static final String STATIONS_Q1_Q2 = "data/Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	public static final String STATIONS_Q3_Q4 = "data/Divvy_Stations_2017_Q3Q4.csv";

	public static final String NODOS_FILE= "data/Nodes_of_Chicago_Street_Lines.txt";



	private CSVReader reader2;


	private GrafoDirigido<Integer, IVertice<Integer>, IArco> grafoc3;
	private GrafoNoDirigido<Integer, IVertice<Integer>, IArco> grafo;
	private GrafoDirigeichon<Integer, VerticeNumerico, Double> grafo2;
	private Lista<VerticeStation> listastation;
	private LinkedList<VerticeStation>listaStations;
	private CSVReader reader1;
	private Lista<Trip>listaTrips;
	private HashTableSeparateChaining<ArcoNumerico, Lista<IVertice<Integer>>>hasharcos;
	private HashTableSeparateChaining<Integer,VerticeStation> heapconcurrencia;
	private LinkedList<VerticeNumerico> listaVertices;
	//private Collection<Integer,IVertice<Integer>,IArco> collection;

	public Manager()
	{



		listastation=new Lista<>();
		grafo=new GrafoNoDirigido<>();
		grafoc3=new GrafoDirigido<>();
		listaTrips=new Lista<>();
		hasharcos=new HashTableSeparateChaining<>();
		heapconcurrencia=new HashTableSeparateChaining<>(600);
		listaStations = new LinkedList<>();
		listaVertices =  new LinkedList<>();
//		grafo2 = new GrafoDirigeichon<>(4000);


	}

	/*
	 * HashTable con los arcos y sus vertices
	 * (non-Javadoc)
	 * @see API.IManager#darArcos()
	 */
	@Override
	public HashTableSeparateChaining<ArcoNumerico, Lista<IVertice<Integer>>> darArcos()
	{
		return hasharcos;
	}

	/*
	 * 
	 * Grafo No dirigido
	 * (non-Javadoc)
	 * @see API.IManager#darGrafo()
	 */
	@Override
	public GrafoNoDirigido<Integer, IVertice<Integer>, IArco> darGrafo()
	{
		return grafo;
	}

	/*
	 * Lista de Viajes
	 * (non-Javadoc)
	 * @see API.IManager#darListaTrips()
	 */
	@Override
	public Lista<Trip> darListaTrips()
	{
		return listaTrips;
	}

	/*
	 * Cargar las intersecciones de chicago
	 */
	public void cargarNodos() throws VerticeYaExisteException
	{
		try {
			FileReader h = new FileReader("./data/Nodes_of_Chicago_Street_Lines.txt");

			BufferedReader bf = new BufferedReader(h);
			String entrada = bf.readLine();
			while (entrada != null) {
				String[] arr = entrada.split(",");
				Integer numNodo = Integer.parseInt(arr[0]);
				double longi = Double.parseDouble(arr[1]);
				double lati = Double.parseDouble(arr[2]);
				VerticeNumerico verti = new VerticeNumerico(numNodo, longi, lati);
				listaVertices.append(verti);
				grafo.agregarVertice( verti);
//				grafo2.addVertex(numNodo, verti);
				entrada = bf.readLine();
			}


			bf.close();
			h.close();
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
		} 

	}

	/*
	 * Cargar los vertices para el grafo dirigido
	 */
	public void cargarVerticeStationsC3()
	{
		String[] lineaLeer2;

		try {
			reader2 = new CSVReader(new FileReader(STATIONS_Q1_Q2));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			lineaLeer2=reader2.readNext();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			while(((lineaLeer2 = reader2.readNext())!=null)) {
				int id=Integer.parseInt(lineaLeer2[0]);
				String nombre= lineaLeer2[1];
				String ciudad=lineaLeer2[2];
				double latitude= Double.parseDouble(lineaLeer2[3]);
				double longitude= Double.parseDouble(lineaLeer2[4]);
				int dpcap=Integer.parseInt(lineaLeer2[5]);
				String onDate=lineaLeer2[6];
				VerticeStation station=new VerticeStation(id, nombre, ciudad, latitude, longitude, dpcap, onDate);						
				grafoc3.agregarVertice(station);
				listastation.agregar(station);
				listaStations.append(station);
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (VerticeYaExisteException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Cargar los vertices para el grafo no dirigido
	 */
	public void cargarVerticeStations()
	{
		System.out.println("entro");		
		String[] lineaLeer2;
		try {
			reader2 = new CSVReader(new FileReader(STATIONS_Q1_Q2));
			lineaLeer2=reader2.readNext();

			while(((lineaLeer2 = reader2.readNext())!=null)) {
				try {
					double distanciaarco=0;
					int idVerticeDestino=0;
					int id=Integer.parseInt(lineaLeer2[0]);
					String nombre= lineaLeer2[1];
					String ciudad=lineaLeer2[2];
					double latitude= Double.parseDouble(lineaLeer2[3]);
					double longitude= Double.parseDouble(lineaLeer2[4]);
					int dpcap=Integer.parseInt(lineaLeer2[5]);
					String onDate=lineaLeer2[6];
					VerticeStation station=new VerticeStation(id, nombre, ciudad, latitude, longitude, dpcap, onDate);						
					Queue<Integer> x= grafo.darVertices().keys();
					for(int i=0;i<x.size();i++){
						double comparar=1000000000;
						double lat=grafo.darVertice(i).darLatitud();
						double lon=grafo.darVertice(i).darLongitud();
						double distancia=getDistance(latitude, longitude, lat, lon);
						if(distancia<=comparar)
						{
							comparar=distancia;
							distanciaarco=comparar;
							idVerticeDestino=grafo.darVertice(i).darId();
						}

					}
					grafo.agregarVertice(station);
					ArcoNumerico arc=new ArcoNumerico(distanciaarco);
					grafo.agregarArco(id, idVerticeDestino,  arc);
					
				}
				catch(Exception e)
				{
					System.out.println("error por" + e.getMessage());
				}
			}
		} catch (IOException e) {
			
			e.printStackTrace();
		}


	}


	/*
	 * Cargar los viajes para el grafo dirigido
	 */
	public void cargarTrips() throws IOException 
	{
		String[] lineaLeer1;
		try {
			reader1 = new CSVReader(new FileReader(TRIPS_Q1));
			lineaLeer1=reader1.readNext();
			while(((lineaLeer1 = reader1.readNext())!=null))
			{
				try {	
					int pid=Integer.parseInt(lineaLeer1[0]);
					String startTime= lineaLeer1[1];
					String endTime= lineaLeer1[2];
					int bikeId=Integer.parseInt(lineaLeer1[3]);
					int tripDuration=Integer.parseInt(lineaLeer1[4]);
					int fromStationId=Integer.parseInt(lineaLeer1[5]);
					String fromStationName=lineaLeer1[6];
					int toStationId=Integer.parseInt(lineaLeer1[7]);
					String toStationName=lineaLeer1[8];
					String userType=lineaLeer1[9];
					String gender=lineaLeer1[10];
					String birth=lineaLeer1[11];
					Trip station2=new Trip(pid, startTime, endTime, bikeId, tripDuration, fromStationId, fromStationName,toStationId, toStationName, userType, gender, birth, 0 );
					listaTrips.agregar(station2);
					IVertice<Integer> origen =  grafoc3.darVertice(fromStationId);
					IVertice<Integer> conexion =  grafoc3.darVertice(toStationId);
					double dist = calculateDistance(conexion.darLatitud(), conexion.darLongitud(), origen.darLongitud(), origen.darLatitud());
					ArcoNumerico infoArco = new ArcoNumerico(dist);
					grafoc3.agregarArco(fromStationId, toStationId, infoArco);
					Lista<IVertice<Integer>> listaAgregar=new Lista<>();
					for(int i=0;i<listastation.darLongitud();i++)
					{
						IVertice<Integer> vertice=listastation.darElemento(i);
						VerticeStation station=listastation.darElemento(i);
						if(vertice.darId()==fromStationId||vertice.darId()==toStationId)
						{

							listaAgregar.agregar(vertice);
						}
						if(station.darId()==fromStationId)
						{
							station.aumentarViajes();
							station.aumentarViajesSalieron();
							heapconcurrencia.put(station.darId(),station);
						}
						if(station.darId()==toStationId)
						{
							station.aumentarViajes();
							station.aumentarViajesLLegaron();
							heapconcurrencia.put(station.darId(),station);

						}
					}
					hasharcos.put(infoArco, listaAgregar);
				}
				catch(Exception e)
				{
				}
			}
		}finally 
		{

		}
	}

	/*
	 * Cargar los arcos para el grafo no dirigido
	 */
	public void cargarAdyacentes() throws VerticeNoExisteException, ArcoYaExisteException
	{
		try {
			FileReader h = new FileReader("./data/Adjacency_list_of_Chicago_Street_Lines.txt");
			BufferedReader bf = new BufferedReader(h);
			bf.readLine();
			bf.readLine();
			bf.readLine();
			String entrada = bf.readLine();
			while (entrada != null) {
				String[] arr = entrada.split(" ");
				int numNodo = Integer.parseInt(arr[0]);
				try {
					for(int i=0;i<arr.length;i++) {
						int nodoConect = Integer.parseInt(arr[i]);
						IVertice<Integer> origen =  grafo.darVertice(numNodo);
						IVertice<Integer> conexion =  grafo.darVertice(nodoConect);
						double dist = calculateDistance(origen.darLatitud(), origen.darLongitud(), conexion.darLongitud(), conexion.darLatitud());
						ArcoNumerico arc = new ArcoNumerico(dist);
						grafo.agregarArco(numNodo, nodoConect,arc);
						}
				}
				catch(Exception t)
				{
					System.out.println("no entro");
				}

				entrada = bf.readLine();
			}

			bf.close();
			h.close();
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}
	
	/*
	 * Se persiste el grafo no dirigido
	 * (non-Javadoc)
	 * @see API.IManager#grafoJson()
	 */
	@Override
	public void grafoJson()
	{
		File jsonVerticesEsta= new File("./data/verticesEstaciones.json");
		Gson gson= new Gson();
		Lista<IVertice<Integer>> verticesIn= grafoc3.darVertices();
		ArrayList<IVertice<Integer>> lista1= new ArrayList<IVertice<Integer>>();
		for(int i =0; i<verticesIn.darLongitud();i++){
			IVertice<Integer> actual= verticesIn.darElemento(i);
			lista1.add(actual);
		}
		try
		{

			if(!jsonVerticesEsta.exists())
			{
				jsonVerticesEsta.createNewFile();
				PrintWriter pw= new PrintWriter(jsonVerticesEsta);
				System.out.println(lista1.size());
				pw.println(gson.toJson(lista1) + "\n");
				pw.close();
			}
		}
		catch (FileNotFoundException e)
		{
			e.getMessage();
		}
		catch (IOException e1)
		{
			e1.getMessage();
		}
	}



	/*
	 * Cargar Grafo no dirigido
	 * (non-Javadoc)
	 * @see API.IManager#cargarDatos()
	 */
	@Override
	public void cargarDatos() {

		try 
		{
			cargarNodos();
		}
		catch (VerticeYaExisteException e) 
		{
			e.printStackTrace();
		}
		try 
		{
			cargarAdyacentes();
		} 
		catch (VerticeNoExisteException e) 
		{
			
			e.printStackTrace();
		} 
		catch (ArcoYaExisteException e) 
		{
			e.printStackTrace();
		}
		cargarVerticeStations();

	}

	/*
	 * Lista con los componentes conexos del grafo dirigido
	 * (non-Javadoc)
	 * @see API.IManager#C2ComponentesConexos()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Lista<ComponenteConexo> C2ComponentesConexos() 
	{

		return grafoc3.darComponenteConexo();
	}

	/*
	 * Estaciones mas congestinadas
	 * (non-Javadoc)
	 * @see API.IManager#StationsMasCongestionadas(int)
	 */
	@Override
	public Lista<VerticeStation> StationsMasCongestionadas(int nStations)
	{
		Lista<VerticeStation>rta=new Lista<>();
		MaxHeapCP<VerticeStation>heap=new MaxHeapCP<>(heapconcurrencia.size());
		boolean termino=false;
		int contador=0;
		for (Integer verticeStation : heapconcurrencia.keys()) 
		{
			heap.add(heapconcurrencia.get(verticeStation));

		}
		for(int i=0;i<heap.length()&&!termino;i++)
		{
			if(contador<=nStations) {
				rta.agregar(heap.poll());
				contador++;
				if(contador>nStations)
				{
					termino=true;
				}
			}
		}
		return rta;

	}

	/*
	 * Ruta minima entre n estaciones
	 * (non-Javadoc)
	 * @see API.IManager#darRutasMinimas(int)
	 */
	@Override
	public ElCamino<Integer, IVertice<Integer>, IArco> darRutasMinimas(int nStations) 	
	{
		Lista<VerticeStation> x =StationsMasCongestionadas(nStations);
		ElCamino<Integer, IVertice<Integer>, IArco> rta = null;
		try {
			System.out.println(grafoc3.hayCamino(x.darElemento(0).darId(), x.darElemento(x.darLongitud()-1).darId()));
			rta = grafoc3.darCaminoMasBarato(x.darElemento(0).darId(), x.darElemento(x.darLongitud()-1).darId());
		} catch (VerticeNoExisteException e) {
			e.printStackTrace();
		}
		return rta;
	}

	/*
	 * Cargar el Grafo Dirigido
	 */
	public void cargarGrafoC3()
	{
		cargarVerticeStationsC3();

		try {
			cargarTrips();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/*
	 *Grafo Dirigido de la parte C
	 */
	@Override
	public GrafoDirigido<Integer, IVertice<Integer>, IArco> grafoC3()
	{
		cargarGrafoC3();
		return grafoc3;
	}

	/*
	 * Calcular la distancia Harvesiana
	 */
	public double calculateDistance (double lat1, double lon1, double longitudReferencia, double latitudReferencia) {
		
		final int R = 6371*1000; // Radious of the earth in meters 
		
		double latDistance = Math.toRadians(latitudReferencia-lat1); 
		
		double lonDistance = Math.toRadians(longitudReferencia-lon1); 
		
		double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))* Math.cos(Math.toRadians(latitudReferencia)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2); 
		
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		
		double distance = R * c;
		return distance; 
		
	}
	
	/*
	 * Calcular la distancia Harvesiana
	 */
	public double getDistance (double lat1, double lon1, double lat2, double lon2)  {       
		final int R = 6371*1000; // Radious of the earth in meters    
		Double latDistance = Math.toRadians(lat2-lat1);     
		Double lonDistance = Math.toRadians(lon2-lon1);     
		Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance/2) *  Math.sin(lonDistance/2);   
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));     Double distance = R * c;  
		return distance;
	}
	
	public IVertice<Integer> darVerticeEstacionMasCercano(double lati, double longi)
	{

		IVertice<Integer> mayor = (IVertice<Integer>) listastation.darElemento(0);
		for (IVertice<Integer> actual : listaStations) {
			if((Math.abs(lati-actual.darLatitud())) < Math.abs(lati-mayor.darLatitud()) && Math.abs(longi-actual.darLongitud()) < Math.abs(longi-mayor.darLongitud()))
			{
				mayor = actual;
			}
		}
		return mayor;

	}

	public VerticeStation darEstacionID(int pIdentificador)
	{
		for (VerticeStation actual : listaStations)
			if(actual.darId() == pIdentificador)
				return actual;

		return null;
	}
	public VerticeNumerico darVerticeInterseccionMasCercano(double lati, double longi)
	{
		VerticeNumerico mayor =  (VerticeNumerico) listaVertices.darPrimero().darElemento();
		for (VerticeNumerico actual : listaVertices) {
			if((Math.abs(lati-actual.darLatitud())) < Math.abs(lati-mayor.darLatitud()) && Math.abs(longi-actual.darLongitud()) < Math.abs(longi-mayor.darLongitud()))
			{
				mayor = actual;
			}
		}
		return mayor;

	}
	
	public LinkedList<String> darViajeMasCorto(double latInicial, double lonInicial, double latFinal, double lonFinal) throws Exception
	{
		// TODO Auto-generated method stub
		try
		{
		LinkedList<String> a2= new LinkedList<>();
		a2.append(latInicial+","+lonInicial);
		
		//Da la estaci�n mas cercana a la latitud y longitud inicial
		IVertice<Integer> salida = darVerticeEstacionMasCercano(latInicial, lonInicial);
		VerticeStation inic = darEstacionID(salida.darId());
		System.out.println("La estaci�n mas cercana al origen es: " + inic.getStationName());

		//Da la estaci�n mas cercana a la latitud y longitud final
		IVertice<Integer> llegada = darVerticeEstacionMasCercano(latFinal, lonFinal);
		VerticeStation fin = darEstacionID(llegada.darId());
		System.out.println("La estaci�n mas cercana al destino es: " + fin.getStationName());

		//Da la distancia estimada
		System.out.println("La distancia estimada es: " + calculateDistance(inic.darLatitud(), inic.darLongitud(), fin.getLongitude(), fin.getLatitude()));

		//Da el vertice mas cercano a la latitud y longitud inicial
		VerticeNumerico vert = darVerticeInterseccionMasCercano(inic.darLatitud(), inic.darLongitud());

		//Da el vertice mas cercano a la latitud y longitud final
		VerticeNumerico vert2 = darVerticeInterseccionMasCercano(fin.darLatitud(), fin.darLongitud());

		BreadthFirstPaths bf = new BreadthFirstPaths(grafo, vert.darId());
		Iterable<Integer> it= bf.pathTo(vert2.darId());
		for (Integer actual : it) {
			System.out.println("Debe pasar por el vertice " + actual);
			VerticeStation estacionIn= darEstacionID(actual);
			a2.append(estacionIn.darLatitud()+","+estacionIn.darLongitud());
		}
		a2.append(latFinal+","+lonFinal);
		return a2;
	}
	catch(Exception e)
	{
		throw new VerticeNoExisteException("Ese no existe", 4);
	}
	}
	public LinkedList<String> darViajeDeCostoMinimo(double latInicial, double lonInicial, double latFinal, double lonFinal) {
		// TODO Auto-generated method stub
		LinkedList<String> a1= new LinkedList<>();
		a1.append(latInicial+","+lonInicial);
		
		//Da la estaci�n mas cercana a la latitud y longitud inicial
		IVertice<Integer> salida = darVerticeEstacionMasCercano(latInicial, lonInicial);
		VerticeStation inic = darEstacionID(salida.darId());
		System.out.println("La estaci�n mas cercana al origen es: " + inic.getStationName());

		//Da la estaci�n mas cercana a la latitud y longitud final
		IVertice<Integer> llegada = darVerticeEstacionMasCercano(latFinal, lonFinal);
		VerticeStation fin = darEstacionID(llegada.darId());
		System.out.println("La estaci�n mas cercana al destino es: " + fin.getStationName());

		//Da el vertice mas cercano a la latitud y longitud inicial
		VerticeNumerico vert = darVerticeInterseccionMasCercano(inic.darLatitud(),inic.darLongitud());

		//Da el vertice mas cercano a la latitud y longitud final
		VerticeNumerico vert2 = darVerticeInterseccionMasCercano(fin.darLatitud(),fin.darLongitud());


//		DjikstraSP dj = new DjikstraSP(grafo2, vert.darId());

		//Existe un camino?
//		Iterable<ArcoG<Integer, Double>> it = dj.pathTo(vert2.darId());
//
//		//Distancia a recorrer
//		System.out.println("La distancia a recorrer es " + dj.distTo(vert2.darId()));
//		int i=0;
//		for (ArcoG<Integer, Double> e : it) {
//			i++;
//			System.out.println("Ir de " + e.getIdI() + " hasta " + e.getIdF());
//			VerticeStation estacionI= darEstacionID(e.getIdI());
//			a1.append(estacionI.darLatitud()+","+estacionI.darLongitud());
//		}
//		a1.append(latFinal+","+lonFinal);
//		System.out.println("Se deben recorrer " + i + " vertices");
		return null;
	}
}
