package model.logic;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import model.data_structures.LinkedList;

public class Dibujar {
	public static void dibujarA1( LinkedList<String> a1)
	{
		try {
			File htmlTemplateFile = new File("data/template/template.html");
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			String scriptTag = "var flightPlanCoordinates = [";
			for(int i=0; i< a1.size(); i++)
			{
				String[] valores=a1.get(i).darElemento().split(",");
				String lat="";
				String lon="";
				lat= valores[0];
				lon=valores[1];
				if(i!=a1.size()-1)
				{
					scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"},";						
				}
				else
				{
					scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"}";						

				}

			}
			scriptTag+="];"
					+ "var flightPath = new google.maps.Polyline({"
					+ "path: flightPlanCoordinates,"
					+ "geodesic: true,"
					+"strokeColor: '#FF0000',"
					+"strokeOpacity: 1.0,"
					+"strokeWeight: 2"
					+"});"
					+"flightPath.setMap(map);";
			for(int i=0; i< a1.size(); i++)
			{
				String[] valores=a1.get(i).darElemento().split(",");
				String lat="";
				String lon="";
				lat= valores[0];
				lon=valores[1];

				scriptTag+= " var cityCircle"+i+ "= new google.maps.Circle({"
						+"strokeColor: '#FF0000',"
						+"strokeOpacity: 0.8,"
						+"strokeWeight: 2,"
						+"fillColor: '#FF0000',"
						+"fillOpacity: 0.35,"
						+"map: map,"
						+"center: {lat:"+ lat +", lng:"+lon+"},"
						+"radius: Math.sqrt(2) * 100"
						+"});";
			}
			htmlString = htmlString.replace("//$script", scriptTag);
			File newHtmlFile = new File("data/template/"+"mapaA1"+".html");
			FileUtils.writeStringToFile(newHtmlFile, htmlString);	
			java.awt.Desktop.getDesktop().browse(newHtmlFile.toURI());
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void dibujarA2(LinkedList<String> a2)
	{
		try {
			File htmlTemplateFile = new File("data/template/template.html");
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			String scriptTag = "var flightPlanCoordinates = [";
			for(int i=0; i< a2.size(); i++)
			{
				String[] valores=a2.get(i).darElemento().split(",");
				String lat="";
				String lon="";
				lat= valores[0];
				lon=valores[1];
				if(i!=a2.size()-1)
				{
					scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"},";						
				}
				else
				{
					scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"}";						

				}

			}
			scriptTag+="];"
					+ "var flightPath = new google.maps.Polyline({"
					+ "path: flightPlanCoordinates,"
					+ "geodesic: true,"
					+"strokeColor: '#FF0000',"
					+"strokeOpacity: 1.0,"
					+"strokeWeight: 2"
					+"});"
					+"flightPath.setMap(map);";
			for(int i=0; i< a2.size(); i++)
			{
				String[] valores=a2.get(i).darElemento().split(",");
				String lat="";
				String lon="";
				lat= valores[0];
				lon=valores[1];

				scriptTag+= " var cityCircle"+i+ "= new google.maps.Circle({"
						+"strokeColor: '#FF0000',"
						+"strokeOpacity: 0.8,"
						+"strokeWeight: 2,"
						+"fillColor: '#FF0000',"
						+"fillOpacity: 0.35,"
						+"map: map,"
						+"center: {lat:"+ lat +", lng:"+lon+"},"
						+"radius: Math.sqrt(2) * 100"
						+"});";
			}
			htmlString = htmlString.replace("//$script", scriptTag);
			File newHtmlFile = new File("data/template/"+"mapaA2"+".html");
			FileUtils.writeStringToFile(newHtmlFile, htmlString);	
			java.awt.Desktop.getDesktop().browse(newHtmlFile.toURI());
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

}
