package model.vo;

public class Calle 
{
	//Attributes
	private String TNODE_ID;				
	private String PRE_DIR;					
	private String STREET_NAM;				
	private String STREET_TYP;				
	private String longitude;				
	private String latitude;				

	//Constructor
	public Calle( )
	{

	}

	//Methods
	public String getTNODE_ID()
	{
		return TNODE_ID;
	}
	
	public String getPRE_DIR()
	{
		return PRE_DIR;
	}
	
	public String getSTREET_NAM()
	{
		return STREET_NAM;
	}
	
	public String getSTREET_TYP()
	{
		return STREET_TYP;
	}
	
	public String getLongitude()
	{
		return longitude;
	}

	public String getLatitude()
	{
		return latitude;
	}
	
	public String setLongitude(String pLongitude)
	{
		return longitude = pLongitude;
	}
	
	public String setLatitude(String pLatitude)
	{
		return latitude = pLatitude;
	}
}
