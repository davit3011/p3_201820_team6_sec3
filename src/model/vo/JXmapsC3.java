package model.vo;


import com.teamdev.jxmaps.Circle;
import com.teamdev.jxmaps.CircleOptions;
import com.teamdev.jxmaps.ControlPosition;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.Polyline;
import com.teamdev.jxmaps.PolylineOptions;
import com.teamdev.jxmaps.swing.MapView;

import model.data_structures.Lista.Lista;
import model.data_structures.grafo.IArco;
import model.data_structures.grafo.IVertice;
import model.data_structures.grafo.VerticeNoExisteException;
import model.data_structures.grafo.grafoDirigido.GrafoDirigido;
import model.data_structures.grafo.grafoDirigido.Vertice;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class JXmapsC3 extends MapView {
	public JXmapsC3(GrafoDirigido<Integer, IVertice<Integer>, IArco> grafo, Lista<Trip> lista, Lista<ComponenteConexo> lista2) {
		setOnMapReadyHandler(new MapReadyHandler() {
			@Override
			public void onMapReady(MapStatus status) {
				if (status == MapStatus.MAP_STATUS_OK) {
					final Map map = getMap();
					MapOptions mapOptions = new MapOptions();
					MapTypeControlOptions controlOptions = new MapTypeControlOptions();
					controlOptions.setPosition(ControlPosition.TOP_RIGHT);
					mapOptions.setMapTypeControlOptions(controlOptions);
					map.setOptions(mapOptions);
					map.setCenter(new LatLng(41.8, -87.8));
					map.setZoom(10.0);

					for (int i=0;i<lista.darLongitud();i++) {	
						Trip t=	lista.darElemento(i);
						VerticeStation origen;
						VerticeStation destino;



						ComponenteConexo rta = null;
						for (int j=0;j<lista2.darLongitud();j++) {
							ComponenteConexo holi=	lista2.darElemento(j);
							Lista<Vertice> verss=holi.darVertices();
							Random ra = new Random();
							int r, g, b;
							r=ra.nextInt(255);
							g=ra.nextInt(255);
							b=ra.nextInt(255);
							Color color = new Color(r,g,b);
							String hex = Integer.toHexString(color.getRGB() & 0xffffff);
							if (hex.length() < 6) {
								hex = "0" + hex;
							}
							hex = "#" + hex;


							for(int k=0;k<verss.darLongitud();k++)
							{
								if(k<verss.darLongitud()-1) {
								IVertice<Integer> actuals=verss.darElemento(k).darInfoVertice();
									IVertice<Integer> destinos=verss.darElemento(k+1).darInfoVertice();
								LatLng[] pathes = {new LatLng(actuals.darLongitud(),actuals.darLatitud()), new LatLng(destinos.darLongitud(),destinos.darLatitud())};
								LatLng circles= new LatLng(actuals.darLongitud(),actuals.darLatitud());
								Polyline polylines = new Polyline(map);
								Circle xs=new Circle(map);
								xs.setCenter(circles);
								double oyeme =(actuals.calcularCantidadViajesPorDia()/(grafo.darNArcos()/90))*100;
								xs.setRadius(oyeme*1000);
								polylines.setPath(pathes);
								CircleOptions options2s = new CircleOptions();
								options2s.setStrokeOpacity(1);
								options2s.setFillColor(hex);

								xs.setOptions(options2s);
								PolylineOptions optionnss = new PolylineOptions();
								optionnss.setGeodesic(true);
								optionnss.setStrokeColor(hex);
								optionnss.setStrokeOpacity(1.0);
								optionnss.setStrokeWeight(2.0);
								polylines.setOptions(optionnss);}
							}

						}











						try {
							Random ra = new Random();
							int r, g, b;
							r=ra.nextInt(255);
							g=ra.nextInt(255);
							b=ra.nextInt(255);
							Color color = new Color(r,g,b);
							String hex = Integer.toHexString(color.getRGB() & 0xffffff);
							if (hex.length() < 6) {
								hex = "0" + hex;
							}
							hex = "#" + hex;
							origen = (VerticeStation) grafo.darVertice(t.getStartStationId());
							destino = (VerticeStation) grafo.darVertice(t.getEndStationId());

							LatLng[] paths = {new LatLng(origen.darLongitud(),origen.darLatitud()), new LatLng(destino.darLongitud(),destino.darLatitud())};
							LatLng circle= new LatLng(origen.darLongitud(),origen.darLatitud());
							Polyline polyline = new Polyline(map);
							Circle x=new Circle(map);
							x.setCenter(circle);
							double oyeme =(origen.calcularCantidadViajesPorDia()/(grafo.darNArcos()/90))*100;
							x.setRadius(oyeme*1000);
							polyline.setPath(paths);
							CircleOptions options2 = new CircleOptions();
							options2.setStrokeOpacity(1);
							options2.setFillColor(hex);

							x.setOptions(options2);
							PolylineOptions optionns = new PolylineOptions();
							optionns.setGeodesic(true);
							optionns.setStrokeColor(hex);
							optionns.setStrokeOpacity(1.0);
							optionns.setStrokeWeight(2.0);
							polyline.setOptions(optionns);}
						catch (VerticeNoExisteException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}


					}
				}
			}
		});
	}



}
