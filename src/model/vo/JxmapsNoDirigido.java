package model.vo;


import com.teamdev.jxmaps.Circle;
import com.teamdev.jxmaps.CircleOptions;
import com.teamdev.jxmaps.ControlPosition;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.Polyline;
import com.teamdev.jxmaps.PolylineOptions;
import com.teamdev.jxmaps.swing.MapView;

import model.data_structures.HashTableSeparateChaining;
import model.data_structures.Lista.Lista;
import model.data_structures.grafo.IArco;
import model.data_structures.grafo.IVertice;
import model.data_structures.grafo.VerticeNoExisteException;
import model.data_structures.grafo.grafoDirigido.GrafoDirigido;
import model.data_structures.grafo.grafoNoDirigido.GrafoNoDirigido;

import javax.swing.*;
import java.awt.*;

public class JxmapsNoDirigido extends MapView {
	public JxmapsNoDirigido(GrafoNoDirigido<Integer, IVertice<Integer>, IArco> grafo) {
		setOnMapReadyHandler(new MapReadyHandler() {
			@Override
			public void onMapReady(MapStatus status) {
				if (status == MapStatus.MAP_STATUS_OK) {
					final Map map = getMap();
					MapOptions mapOptions = new MapOptions();
					MapTypeControlOptions controlOptions = new MapTypeControlOptions();
					controlOptions.setPosition(ControlPosition.TOP_RIGHT);
					mapOptions.setMapTypeControlOptions(controlOptions);
					map.setOptions(mapOptions);
					map.setCenter(new LatLng(41.8, -87.8));
					map.setZoom(10.0);

					HashTableSeparateChaining<IArco,Lista< IVertice<Integer>>> adyacentes =  grafo.darMatriz(); 
					for (IArco iter : adyacentes.keys()) 
					{
						    
						Lista<IVertice<Integer>>listaVertices=adyacentes.get(iter);
						IVertice<Integer> origen=listaVertices.darElemento(0);
						IVertice<Integer> destino=listaVertices.darElemento(1);
						LatLng circle= new LatLng(origen.darLatitud(),origen.darLongitud());LatLng circle2=new LatLng(destino.darLatitud(),destino.darLongitud());
						Circle x=new Circle(map);
						Circle x2 = new Circle(map);
						x2.setCenter(circle2);
						x2.setRadius(40);
						x.setCenter(circle);
						x.setRadius(40);
						CircleOptions options3=new CircleOptions();
						CircleOptions options2 = new CircleOptions();
						options3.setStrokeOpacity(1);
						options2.setStrokeOpacity(1);
						options3.setFillColor("#FF1000");
						options2.setFillColor("#FF1000");
						x2.setOptions(options3);
						x.setOptions(options2);
						LatLng[] paths = {new LatLng(origen.darLatitud(),origen.darLongitud()), new LatLng(destino.darLatitud(),destino.darLongitud())};
						Polyline polyline = new Polyline(map);
						polyline.setPath(paths);
						
						PolylineOptions optionns = new PolylineOptions();
						optionns.setGeodesic(true);
						optionns.setStrokeColor("#FF0000");
						optionns.setStrokeOpacity(1.0);
						optionns.setStrokeWeight(2.0);
						polyline.setOptions(optionns);
					
					}
					
					
					
					
					


				


				}
			}
		
	});
}



}
