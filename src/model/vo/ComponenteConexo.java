package model.vo;

import model.data_structures.Lista.Lista;
import model.data_structures.grafo.IVertice;
import model.data_structures.grafo.grafoDirigido.Vertice;

public class ComponenteConexo<K,V,A> implements Comparable<ComponenteConexo> {
	private String color;
	private int numVertices;
	private Lista<Vertice> vertices;
	
	
	public ComponenteConexo(String color) {
		super();
		this.color = color;
//		Inicializar  Lista
	}
	
	public ComponenteConexo(String color, Lista<Vertice> vertices) {
		super();
		this.color = color;
		this.numVertices = vertices.darLongitud();
		this.vertices = vertices;
	}
	public Lista<Vertice> darVertices()
	{
		return vertices;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getNumVertices() {
		return numVertices;
	}
	public void setNumVertices(int numVertices) {
		this.numVertices = numVertices;
	}
	
	/**
     * Agrega vertice al listado
     * @param vertice
     */
    public void agregarVerticeQueLlega(Vertice vertice){
        vertices.agregar(vertice);
        numVertices++;
    }
	
	@Override
	public int compareTo(ComponenteConexo o) {
		return Integer.compare(this.getNumVertices(), o.getNumVertices());
	}
	
	
	
	

}
