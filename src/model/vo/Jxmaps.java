package model.vo;


import com.teamdev.jxmaps.Circle;
import com.teamdev.jxmaps.CircleOptions;
import com.teamdev.jxmaps.ControlPosition;
import com.teamdev.jxmaps.InfoWindow;
import com.teamdev.jxmaps.InfoWindowOptions;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.Marker;
import com.teamdev.jxmaps.Polyline;
import com.teamdev.jxmaps.PolylineOptions;
import com.teamdev.jxmaps.swing.MapView;

import model.data_structures.ArcoNumerico;
import model.data_structures.HashTableSeparateChaining;
import model.data_structures.Lista.Lista;
import model.data_structures.grafo.IArco;
import model.data_structures.grafo.IVertice;
import model.data_structures.grafo.VerticeNoExisteException;
import model.data_structures.grafo.grafoDirigido.GrafoDirigido;
import model.data_structures.grafo.grafoDirigido.Vertice;

import javax.swing.*;
import java.awt.*;


public class Jxmaps extends MapView {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Jxmaps(GrafoDirigido<Integer, IVertice<Integer>, IArco> grafo, Lista<ComponenteConexo> lista, HashTableSeparateChaining<ArcoNumerico, Lista<IVertice<Integer>>> hash) {
		setOnMapReadyHandler(new MapReadyHandler() {
			@Override
			public void onMapReady(MapStatus status) {
				if (status == MapStatus.MAP_STATUS_OK) {
					final Map map = getMap();
					MapOptions mapOptions = new MapOptions();
					MapTypeControlOptions controlOptions = new MapTypeControlOptions();
					controlOptions.setPosition(ControlPosition.TOP_RIGHT);
					mapOptions.setMapTypeControlOptions(controlOptions);
					map.setOptions(mapOptions);
					map.setCenter(new LatLng(41.8, -87.8));
					map.setZoom(10.0);


					int comparar=0;
					ComponenteConexo rta = null;
					for (int i=0;i<lista.darLongitud();i++) {	
						ComponenteConexo t=	lista.darElemento(i);

						int x=t.getNumVertices();
						if(x>comparar) {
							comparar=x;
							rta=t;
						}
					}
					//aca ya esta el unico componente conexo mas grande
					Lista<Vertice> lst=rta.darVertices();
					//aca doy los vertices de ese componente conexo
					for(int j=0;j<lst.darLongitud();j++) {
						IVertice<Integer> origen=lst.darElemento(j).darInfoVertice();
						IVertice<Integer> destino=lst.darElemento(j+1).darInfoVertice();
						String contentString = "<table cellpadding=\"5\"><tr><td><img src=\""  + "\" /></td><td valign='top'><p><b>Chicago</b></p>" +
								"<p>El id del vertice apuntado.</p>:" +   origen.darId()+"Latitud:"+origen.darLatitud()+"Longitud:"+origen.darLongitud()+
								"<p style=\"color:#757575\">Use InfoWindow to display custom information, related to a point on a map. InfoWindow layout can be formatted using HTML.</p>" +
								"</td></tr></table>";
						LatLng circle= new LatLng(origen.darLongitud(),origen.darLatitud());
						Circle x=new Circle(map);
						x.setCenter(circle);
						x.setRadius(40);
						CircleOptions options2 = new CircleOptions();
						options2.setStrokeOpacity(1);
						options2.setFillColor("#FF1400");
						x.setOptions(options2);
						LatLng[] paths = {new LatLng(destino.darLongitud(),destino.darLatitud()), new LatLng(origen.darLongitud(),origen.darLatitud())};
						Polyline polyline = new Polyline(map);
						polyline.setPath(paths);
						PolylineOptions optionns = new PolylineOptions();
						optionns.setGeodesic(true);
						optionns.setStrokeColor(rta.getColor());
						optionns.setStrokeOpacity(1.0);
						optionns.setStrokeWeight(2.0);
						polyline.setOptions(optionns);
						 final Marker marker = new Marker(map);
		                    // Moving marker to the map center
		                    marker.setPosition(circle);
		                    marker.setVisible(true);
		                    marker.setClickable(true);;
		                    // Creating an information window
		                    final InfoWindow window = new InfoWindow(map);		        
		                    // Setting html content to the information window
		                    window.setContent(contentString);
		                    // Showing the information window on marker
		                    window.open(map, marker);	
					}}
					}

					


						

					




				
			

		});
	}



}
