package model.vo;

public class Cicloruta  implements Comparable<Cicloruta>
{
	private String sid;

	private String id;
	
	private int position;
	
	private long created_at;
	private long updated_at;
	
	
	private String meta;
	
	private String bikeroute;
	
	private double latitudInicial;
	
	
	private double longitudInicial;
	
	
	private double latitudFinal;
	
	private double longitudFinal;

	private int type;
	
	private String street;
	
	private String f_street;
	
	private String t_street;
	
	private String status;
	
	private double shape_leng;
	
	private String[] recorrido;
	


	public Cicloruta(String sid, String id, int position, long created_at, long updated_at, String meta, String bikeroute, int type, String street,
			String f_street, String t_street, String status, double shape_leng, double lonInicial, double latInicial, double lonFinal, double latFinal, String[] recorrido) {
		this.sid = sid;
		this.id = id;
		this.position = position;
		this.created_at = created_at;
		this.updated_at = updated_at;
		this.meta = meta;
		this.bikeroute = bikeroute;
		this.type = type;
		this.street = street;
		this.f_street = f_street;
		this.t_street = t_street;
		this.status = status;
		this.shape_leng = shape_leng;
		
		this.latitudFinal=latFinal;
		
		this.latitudInicial=latInicial;
		
		this.longitudInicial=lonInicial;
		
		this.longitudFinal=lonFinal;
		
		this.recorrido=recorrido;
	}
	
	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public long getCreated_at() {
		return created_at;
	}

	public void setCreated_at(long created_at) {
		this.created_at = created_at;
	}

	

	public long getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(long updated_at) {
		this.updated_at = updated_at;
	}


	public String getMeta() {
		return meta;
	}

	public void setMeta(String meta) {
		this.meta = meta;
	}

	public String getBikeroute() {
		return bikeroute;
	}

	public void setBikeroute(String bikeroute) {
		this.bikeroute = bikeroute;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getF_street() {
		return f_street;
	}

	public void setF_street(String f_street) {
		this.f_street = f_street;
	}

	public String getT_street() {
		return t_street;
	}

	public void setT_street(String t_street) {
		this.t_street = t_street;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getShape_leng() {
		return shape_leng;
	}

	public void setShape_leng(double shape_leng) {
		this.shape_leng = shape_leng;
	}
	
	public double getLatitudInicial() {
		return latitudInicial;
	}

	public void setLatitudInicial(double latitudInicial) {
		this.latitudInicial = latitudInicial;
	}

	public double getLongitudInicial() {
		return longitudInicial;
	}

	public void setLongitudInicial(double longitudInicial) {
		this.longitudInicial = longitudInicial;
	}

	public double getLatitudFinal() {
		return latitudFinal;
	}

	public void setLatitudFinal(double latitudFinal) {
		this.latitudFinal = latitudFinal;
	}

	public double getLongitudFinal() {
		return longitudFinal;
	}

	public void setLongitudFinal(double longitudFinal) {
		this.longitudFinal = longitudFinal;
	}
	
	public String[] getRecorrido() {
		return recorrido;
	}

	public void setRecorrido(String[] recorrido) {
		this.recorrido = recorrido;
	}

	@Override
	public int compareTo(Cicloruta o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
