package model.vo;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

public class TripDistancia implements Comparable<TripDistancia> {

	public final static String MALE = "male";
	public final static String FEMALE = "female";
	public final static String UNKNOWN = "unknown";

	private int tripId;
	private Date startTime;
	private Date stopTime;
	private int bikeId;
	private int tripDuration;
	private int startStationId;
	private String startStationName;
	private int endStationId;
	private String endStationName;
	private String usertype;
	private String gender;
	private String birthYear;
	private double distancia;

	public TripDistancia(int tripId ,Date pStartTime,Date pStopTime, int pBikeId, int pTripDuration, int startStationId, String pStartStationName, int endStationId, String pEndStationName, String pUsertype, String gender, String birthYear,double pdistancia) {
		this.tripId = tripId;
		this.startTime = pStartTime;
		this.stopTime = pStopTime;
		this.bikeId = pBikeId;
		this.tripDuration = pTripDuration;
		this.startStationId = startStationId;
		this.endStationId = endStationId;
		this.gender = gender;
		this.startStationName=pStartStationName;
		this.endStationName=pEndStationName;
		this.usertype=pUsertype;
		this.birthYear=birthYear;
		this.distancia=pdistancia;

	}

	@Override
	public int compareTo(TripDistancia o) {

if(this.darDistancia()==o.darDistancia())
{
	return 0;
}
if(this.darDistancia()<=o.darDistancia())
{
	return -1;
}
else return 1;
	}
	public double darDistancia()
	{
		return distancia;
	}
	public void cambiarDistancia(double pdistancia)
	{
		distancia=pdistancia;
	}
	public String getBirthYear()
	{
		return birthYear;
	}
	public String getUsertype()
	{
		return usertype;
	}
	public String getEndStationName()
	{
		return endStationName;
	}
	public int getTripId() {
		return tripId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public Date getStopTime() {
		return stopTime;
	}
	public String getStartStationName()
	{
		return startStationName;
	}
	public int getBikeId() {
		return bikeId;
	}

	public int getTripDuration() {
		return tripDuration;
	}

	public int getStartStationId() {
		return startStationId;
	}

	public int getEndStationId() {
		return endStationId;
	}

	public String getGender() {
		return gender;
	}
	
	public Date setFecha(String pFecha)
	{
		Calendar calendario = Calendar.getInstance();
		String all [] = pFecha.split(" ");
		String fecha [] = all[0].split("/");
		String hora [] = all[1].split(":");
		
		int mes = Integer.parseInt(fecha[0]);
		int dia = Integer.parseInt(fecha[1]);
		int anio = Integer.parseInt(fecha[2]);
		
		int hour = Integer.parseInt(hora[0]);
		int minuto = Integer.parseInt(hora[1]);
		int segundo =  Integer.parseInt(hora[2]);
		calendario.set(anio, mes, dia, hour, minuto, segundo);
		Date fechaFinal = calendario.getTime();
		return fechaFinal;
		}
}
