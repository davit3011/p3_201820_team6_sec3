package model.vo;


import com.teamdev.jxmaps.Circle;
import com.teamdev.jxmaps.CircleOptions;
import com.teamdev.jxmaps.ControlPosition;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.Polyline;
import com.teamdev.jxmaps.PolylineOptions;
import com.teamdev.jxmaps.swing.MapView;

import model.data_structures.Lista.Lista;
import model.data_structures.grafo.ArcoNoExisteException;
import model.data_structures.grafo.IArco;
import model.data_structures.grafo.IVertice;
import model.data_structures.grafo.VerticeNoExisteException;
import model.data_structures.grafo.grafoDirigido.GrafoDirigido;

import javax.swing.*;
import java.awt.*;

public class JXmapsc23 extends MapView {
	public JXmapsc23(GrafoDirigido<Integer, IVertice<Integer>, IArco> grafo, Lista<Trip> lista) {
		setOnMapReadyHandler(new MapReadyHandler() {
			@Override
			public void onMapReady(MapStatus status) {
				if (status == MapStatus.MAP_STATUS_OK) {
					final Map map = getMap();
					MapOptions mapOptions = new MapOptions();
					MapTypeControlOptions controlOptions = new MapTypeControlOptions();
					controlOptions.setPosition(ControlPosition.TOP_RIGHT);
					mapOptions.setMapTypeControlOptions(controlOptions);
					map.setOptions(mapOptions);
					map.setCenter(new LatLng(41.8, -87.8));
					map.setZoom(10.0);

					for (int i=0;i<lista.darLongitud();i++) {	
						Trip t=	lista.darElemento(i);
						IVertice<Integer> origen;
						IVertice<Integer> destino;


						try {
							origen = grafo.darVertice(t.getStartStationId());
							destino = grafo.darVertice(t.getEndStationId());
							LatLng[] paths = {new LatLng(origen.darLongitud(),origen.darLatitud()), new LatLng(destino.darLongitud(),destino.darLatitud())};
							LatLng circle= new LatLng(origen.darLongitud(),origen.darLatitud());
							Polyline polyline = new Polyline(map);
							Circle x=new Circle(map);
							x.setCenter(circle);
							x.setRadius(40);
							polyline.setPath(paths);
							CircleOptions options2 = new CircleOptions();
							options2.setStrokeOpacity(1);
							options2.setFillColor("#FF1000");

							x.setOptions(options2);
							PolylineOptions optionns = new PolylineOptions();
							optionns.setGeodesic(true);
							optionns.setStrokeColor("#FF0000");
							optionns.setStrokeOpacity(1.0);
							try {
								optionns.setStrokeWeight(grafo.darArco(t.getStartStationId(), t.getEndStationId()).darPesoODistancia()/700);
							} catch (ArcoNoExisteException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							polyline.setOptions(optionns);
						} catch (VerticeNoExisteException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}


					}
				}
			}
		});
	}



}
